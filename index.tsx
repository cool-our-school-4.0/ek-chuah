import { AppRegistry } from 'react-native';
import App from './App';
import { name as appName } from './app.json';
import React, { ReactElement } from 'react';
import { Provider as PaperProvider, Colors, DefaultTheme } from 'react-native-paper';

/**
 * This loads the root component of the app
 *
 * @function
 * @returns {ReactElement} Root Component of the App
 * @author coolourschool 4.0
 */
export default function Main(): ReactElement {
    const customTheme = {
        ...DefaultTheme,
        roundness: 4,
        colors: {
            ...DefaultTheme.colors,
            primary: Colors.blue900,
            accent: Colors.cyan500,
            background: '#f6f6f6',
            surface: Colors.white,
            error: '#B00020',
            text: Colors.black,
            onBackground: '#000000',
            onSurface: '#000000'
        }
    };

    return (
        <PaperProvider theme={customTheme}>
            <App />
        </PaperProvider>
    );
}

AppRegistry.registerComponent(appName, () => Main);
