module.exports = {
    plugins: ['jsdoc'],
    parser: '@typescript-eslint/parser', // Specifies the ESLint parser
    extends: [
        'plugin:import/errors',
        'plugin:import/warnings',
        'plugin:import/typescript',
        'plugin:jsdoc/recommended',
        'plugin:react/recommended',
        'plugin:@typescript-eslint/recommended',
        'plugin:@typescript-eslint/eslint-recommended',
        'prettier/@typescript-eslint',
        'plugin:prettier/recommended'
    ],
    parserOptions: {
        ecmaVersion: 2020, // Allows for the parsing of modern ECMAScript features
        sourceType: 'module', // Allows for the use of imports
        ecmaFeatures: {
            jsx: true // Allows for the parsing of JSX
        }
    },
    rules: {
        'comma-dangle': ['error', 'never'],
        '@typescript-eslint/no-explicit-any': 2,
        'jsdoc/check-values': 2,
        '@typescript-eslint/interface-name-prefix': 'off',
        '@typescript-eslint/no-inferrable-types': 'off'
    },
    settings: {
        react: {
            version: 'detect' // Tells eslint-plugin-react to automatically detect the version of React to use
        },
        'import/ignore': ['react-native', 'geojson', 'react'],
        'import/resolver': {
            typescript: {
                alwaysTryTypes: true
            },
            'eslint-import-resolver-typescript': 'true'
        },
        'import/parsers': {
            '@typescript-eslint/parser': ['.ts', '.tsx']
        }
    }
};
