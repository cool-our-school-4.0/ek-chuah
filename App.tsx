import React, { Component, ReactElement } from 'react';
import SplashScreen from 'react-native-splash-screen';
import StackNavigator from './src/components/stack-navigator';

/**
 * This is the entry of the app
 * Wrapper for Navigator returning the sites
 *
 * @class
 * @author coolourschool 4.0
 * @augments Component
 */
export default class App extends Component {
    componentDidMount(): void {
        SplashScreen.hide();
    }

    /**
     * The Entrypoint, this returns what the App looks like.
     * A StackNavigator is the Root Component of our App
     *
     * @function
     * @author coolourschool 4.0
     * @returns {ReactElement} View
     */
    public render(): ReactElement {
        return <StackNavigator></StackNavigator>;
    }
}
