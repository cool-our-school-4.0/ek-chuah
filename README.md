# Ek-Chuah
[![Pipeline
Status](https://gitlab.com/cool-our-school-4.0/ek-chuah/badges/master/pipeline.svg)](https://gitlab.com/cool-our-school-4.0/ek-chuah/-/commits/master)
[![Quality gate](https://sonarcloud.io/api/project_badges/quality_gate?project=cool-our-school-4.0_ek-chuah)](https://sonarcloud.io/dashboard?id=cool-our-school-4.0_ek-chuah)

## Etwas ueber das Projekt
## Noch etwas ueber das Projekt



## Developer Area

### Einrichtung der Entwicklungsumgebung - Android
Für die Entwicklung der App für Android Geräte muss folgendes vorhanden sein:

* Android SDK

* Android Emulator oder Handy (AVD)

Dafür lässt sich beispielsweise [```Android Studio```](https://developer.android.com/studio/ "Android Studio") installieren.
Durch die Installation wird auch der Android SDK Manager sowie ein Android
Emulator installiert.
[Hier](https://developer.android.com/studio/run/managing-avds "Create and manage
virtual devices") ist beschrieben, wie sich ein virtuelles Gerät mithilfe von
Android Studio erstellen lässt.

### Installieren der Dependencies
Durch ```yarn install``` werden die Dependencies des Projekts lokal installiert

### Starten der App im Debug Modus
Hierfür sollte zunächst der React Native Server gestartet werden:

``` sh
$ react-native start
# or
$ yarn start
```

Hierdurch wird der Server gestartet, über welchen dann neue Änderungen an eine
laufende App im Debug Modus übermittelt wird.
Die App wird nun gestartet durch:

``` sh
$ react-native run-android
# or
$ yarn android
```

### Einrichtung der Entwicklungsumgebung - iOS
Hierzu gibt es derzeit keine Dokumentation, dafür ist ein Mac & XCode notwendig.
Beides ist zurzeit nicht vorhanden.

### SonarLint
In der Entwicklung von ```Ek-Chuah``` wird die Codebasis bezüglich Sicherheit,
Bugs, Effektivität und Wartbarkeit durch SonarQube analysiert.
In der IDE kann hierfür SonarLint, ein Plugin für Eclipse, VSCode und weitere
IDEs installiert werden. So erhält man on-the-fly Verbesserungsvorschläge.

#### Setup SonarLint VSCode
1. SonarLint Extension in der IDE installieren
2. Evtl. muss der Java Pfad noch angepasst werden
3. In der User settings.json von VSCode (Windows: ```%APPDATA%\Code\User\settings.json```) folgendes hinzufügen

```json
"sonarlint.connectedMode.connections.sonarqube": [
    {
        "serverUrl": "https://sonarcloud.io",
        "token": "dein token ..."
    }
]
```

#### Troubleshooting
* ```yarn android``` schlägt mit Fehlermeldung "SDK konnte nicht gefunden
  werden" fehl

Für dieses Problem gibt es verschiedene Lösungen.
Die Lösung, die ich verwende setzt darauf, die Android Studio Tools im Pfad
verfügbar zu machen.
Unter Linux geschieht durch folgenden Block in der ```.bashrc```:

``` sh
export ANDROID_HOME=~/Android/Sdk/
export PATH=$PATH:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools
```

Der Pfad, unter welchem sich die SDK befindet, lässt sich in Android Studio im
SDK Manager finden.

In Windows lässt sich das in den Umgebungsvariablen über eine GUI einstellen.
[Hier](https://docs.alfresco.com/4.2/tasks/fot-addpath.html "Adding folder paths
to the Windows path variable") findet sich eine Beschreibung des Vorgangs.


* ```yarn android``` schlägt mit Fehlermeldung "Lizenz für SDK nicht akzeptiert"
  fehl

Wenn SDKs über den SDKManager installiert werden wird man aufgefordert, den
Lizenzen zuzustimmen. Trotzdem ist es aufgetreten, dass Lizenzen noch nicht als
akzeptiert registriert wurden.
Mittels

``` sh
$ yes | ${ANDROID_HOME}/tools/bin/sdkmanager --licenses
# or
$ ${ANDROID_HOME}/tools/bin/sdkmanager --licenses
```

können alle Lizenzen akzeptiert werden. Beim unteren muss manuell zugestimmt werden.

* ```yarn android``` bzw. der Gradle build schlägt mit Fehlermeldung "General error during semantic analysis: Unsupported class file major version 57" fehl

Gradle Versionen < Version 6 unterstützen das Java Runtime Environment 13 nicht, empfohlen wird die Installation von Version 11 (LTS) 

* ```yarn android``` schlägt mit der Fehlermeldung "./android/app/debug.keystroke not found for signing config 'debug'"

Die App muss noch signiert werden, dafür muss noch folgender Befehl ausgeführt werden:

``` sh
$ keytool -genkey -v -keystore android/app/debug.keystore -storepass android -alias androiddebugkey -keypass android -keyalg RSA -keysize 2048 -validity 10000 -dname "CN=debug, OU=coolourschoolv4, C=DE"
```



