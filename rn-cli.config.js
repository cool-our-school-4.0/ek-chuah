module.exports = {
    getTransformModulePath(): string {
        return require.resolve('react-native-typescript-transformer');
    },
    getSourceExts(): [string] {
        return ['ts', 'tsx'];
    }
};
