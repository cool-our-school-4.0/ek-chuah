package com.ekchuah;

import android.location.Location;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.lifecycle.LifecycleObserver;
import com.facebook.react.bridge.LifecycleEventListener;
import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.annotations.ReactProp;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.mapbox.api.directions.v5.DirectionsCriteria;
import com.mapbox.api.directions.v5.models.DirectionsResponse;
import com.mapbox.api.directions.v5.models.DirectionsRoute;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions;
import com.mapbox.mapboxsdk.location.LocationComponentOptions;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.location.modes.RenderMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.services.android.navigation.ui.v5.route.NavigationMapRoute;
import com.mapbox.services.android.navigation.v5.navigation.NavigationRoute;
import com.mapbox.services.android.navigation.v5.navigation.NavigationWalkingOptions;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NavigationViewManager extends SimpleViewManager<LinearLayout> implements LifecycleObserver{
    private static final String LATITUDE = "latitude";
    private static final String LONGITUDE = "longitude";
    private static final String REACT_CLASS = "NavigationView";
    private static final int INITIAL_ZOOM = 14;

    private ThemedReactContext context;
    private String locationString;

    @ReactProp(name="locs")
    public void setLocs(View view, String locs) {
        locationString = locs;
    }


    @NotNull
    @Override
    public String getName() {
        return REACT_CLASS;
    }

    @NotNull
    @Override
    protected LinearLayout createViewInstance(ThemedReactContext reactContext) {
        context = reactContext;
        Mapbox.getInstance(context, BuildConfig.MAPBOX_TOKEN);
        LinearLayout view = (LinearLayout) LayoutInflater.from(reactContext.getCurrentActivity()).inflate(R.layout.map,null);
        context.setTheme(R.style.CustomInstructionView);
        MapView mapView = view.findViewById(R.id.navigationMapView);
        mapView.onCreate(null);
        initializeLifeCycle(mapView);
        mapView.getMapAsync(mapboxMap -> mapboxMap.setStyle(Style.MAPBOX_STREETS, style -> {
            NavigationMapRoute navigationMapRoute = new NavigationMapRoute(mapView,mapboxMap);
            try {
                NavigationRoute.Builder builder = getNavBuilder();
                builder.build()
                   .getRoute(new Callback<DirectionsResponse>() {
                    @Override
                    public void onResponse(@NotNull Call<DirectionsResponse> call, @NotNull Response<DirectionsResponse> response) {
                        if(response.body() != null){
                            DirectionsRoute directionsRoute = response.body().routes().get(0);
                            navigationMapRoute.addRoute(directionsRoute);
                            setDistanceAndDuration(directionsRoute,view);
                            setCamera(mapboxMap,style,view);                                    }
                    }
                    @Override
                    public void onFailure(@NotNull Call<DirectionsResponse> call, Throwable t) {
                        Toast.makeText(context, "Fehler beim Laden der Route! Überprüfe deine Internetverbindung!",
                                Toast.LENGTH_LONG).show();
                    }
                });
            }catch (Exception e){
                Toast.makeText(context, e.getMessage(),
                        Toast.LENGTH_LONG).show();
            }
        }));
        return view;
    }

    private NavigationRoute.Builder getNavBuilder() throws JSONException {
        JSONArray locationsJSON = new JSONArray(locationString);
        Point originCoords = getPointFromJSON(locationsJSON.getJSONObject(0));
        Point destCoords = getPointFromJSON(locationsJSON.getJSONObject(locationsJSON.length()-1));
        NavigationRoute.Builder builder = NavigationRoute.builder(context)
                .accessToken(BuildConfig.MAPBOX_TOKEN)
                .origin(originCoords)
                .destination(destCoords);
        for(int i = 1;i<locationsJSON.length();i++){
            JSONObject jsonLoc = locationsJSON.getJSONObject(i);
            Point point = getPointFromJSON(jsonLoc);
            builder.addWaypoint(point);
        }
        builder.profile(DirectionsCriteria.PROFILE_WALKING);
        NavigationWalkingOptions walkingOptions = NavigationWalkingOptions.builder().walkingSpeed(0.84).build();
        builder.walkingOptions(walkingOptions);

        return builder;
    }

    private Point getPointFromJSON(JSONObject jsonPoint) throws JSONException {
        return  Point.fromLngLat(jsonPoint.getDouble(LONGITUDE),jsonPoint.getDouble(LATITUDE));
    }

    private void setDistanceAndDuration(DirectionsRoute directionsRoute, LinearLayout view){
        int duration = Objects.requireNonNull(directionsRoute.duration()).intValue();
        int hours = duration / 3600;
        int minutes = (duration%3600) / 60;

        int distance = Objects.requireNonNull(directionsRoute.distance()).intValue();

        TextView durationText = view.findViewById(R.id.duration);
        durationText.setText(String.format("Dauer: %dh %dmin", hours, minutes));
        TextView distanceText = view.findViewById(R.id.distance);
        distanceText.setText(String.format("Distanz: %s m",distance ));
    }

    private void setCamera(MapboxMap mapboxMap,Style style, LinearLayout view ){
        try {
            JSONArray locationsJSON = new JSONArray(locationString);
            JSONObject origin = locationsJSON.getJSONObject(0);
            Point originCoords = Point.fromLngLat(origin.getDouble(LONGITUDE),origin.getDouble(LATITUDE));
            CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(originCoords.latitude(),originCoords.longitude())).zoom(INITIAL_ZOOM).build();
            mapboxMap.setCameraPosition(cameraPosition);
            FloatingActionButton relocateBtn = view.findViewById(R.id.relocateCamera);
            initializeLocationComp(style,mapboxMap,relocateBtn);
        }catch (Exception e){
            Toast.makeText(context, e.getMessage(),
                    Toast.LENGTH_LONG).show();
        }
    }

    private  void initializeLocationComp(Style style, MapboxMap mapboxMap, FloatingActionButton relocateBtn){
        try {
            LocationComponentOptions locationComponentOptions = LocationComponentOptions.builder(context).build();
            LocationComponentActivationOptions locationComponentActivationOptions = LocationComponentActivationOptions
                    .builder(context,style)
                    .locationComponentOptions(locationComponentOptions)
                    .useDefaultLocationEngine(true)
                    .build();
            LocationComponent locationComponent = mapboxMap.getLocationComponent();
            locationComponent.onStart();
            locationComponent.activateLocationComponent(locationComponentActivationOptions);
            locationComponent.setLocationComponentEnabled(true);
            locationComponent.setCameraMode(CameraMode.TRACKING);
            locationComponent.setRenderMode(RenderMode.COMPASS);
            setRelocateButton(relocateBtn, mapboxMap,locationComponent);

        }catch (Exception e){
            Toast.makeText(context,"Der Standort kann aufgrund der aktuellen Berechtigungen nicht angezeigt werden!",Toast.LENGTH_LONG).show();
            relocateBtn.setOnClickListener(v -> Toast.makeText(context,"Die Standortdienste sind ausgeschalten",Toast.LENGTH_LONG).show());
        }

    }

    private void setRelocateButton(FloatingActionButton relocateButton, MapboxMap mapboxMap, LocationComponent locationComponent){
        relocateButton.setOnClickListener(v -> {
            try {
                Location lastLoc = locationComponent.getLastKnownLocation();
                assert lastLoc != null;
                CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(lastLoc.getLatitude(),lastLoc.getLongitude())).zoom(INITIAL_ZOOM).build();
                mapboxMap.setCameraPosition(cameraPosition);
            }catch (Exception e){
                Toast.makeText(context,"Die Standortdienste sind ausgeschalten",Toast.LENGTH_LONG).show();
            }

        });
    }

    private void initializeLifeCycle(MapView mapView){
        LifecycleEventListener lifecycleEventListener = new LifecycleEventListener() {
            @Override
            public void onHostResume() {
                mapView.onResume();
            }
            @Override
            public void onHostPause() {
                mapView.onPause();
            }
            @Override
            public void onHostDestroy() {
                mapView.onDestroy();
            }
        };
        context.addLifecycleEventListener(lifecycleEventListener);
    }
}
