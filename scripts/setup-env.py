#!/usr/bin/env python3
'''
This script sets up the environment for building Ek Chuah
'''
print('Setting up Ek Chuah for your use...')
import os.path
if os.path.isfile('./env.json'):
    print('Great! Everything seems to be setup!')
    print('If you need to reset your setup, please delete the file \'env.json\' in the root of the project directory')
    print('Or, if you feel comfortable, you can make the changes neccesary yourself')

print('********************************************************')
print('Setting up API Key for access to Mapbox API and OSM Maps')
print('Please enter your API Key for Mapbox. It will not be echoed')
try:
    import getpass
    key = getpass.getpass(prompt='API Key: ')
except Exception as error:
    print('An error occured: ', error)

import json
f = open('./env.json', 'w+')
dataset = {'accessToken': key}
f.write(json.dumps(dataset))
