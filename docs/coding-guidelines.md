## Offizielle Coding - Guidelines v1.0

### Naming Conventions

-   **Klassen** immer in **P**ascal**C**ase
-   **Funktionen** bei Exporten in **P**ascal**C**ase
-   **Funktionen** bei Sonstigem in **c**amel**C**ase
-   **Variablen** in **c**amel**C**ase
-   **Enum** in **P**ascal**C**ase, **c**amel**C**ase für Member
-   **Interfaces** in **P**ascal**C**ase, **c**amel**C**ase für Member
-   **Types** in **P**ascal**C**ase, **c**amel**C**ase für Member
-   **Dateinamen** in kebap-case
-   **Private Klassenvariablen** mit einem Unterstrich vornedran

### Sonstiges

-   Über jede Funktion / Klasse / Typ ein Kommentar nach folgendem Template:
    ```
    /**
     * Beschreibung
     *
     * @author 'Author'
     * @returns {Typ} Beschreibung
     * @function/class/interface
     * @param {Typ} Paramname Beschreibung
     */
    ```

    Weitere mögliche Tags: ```@public``` / ```@private```

-   Bei funktionalen Änderungen muss zusätzlich ein Kommentar im Funktionsheader hinzugefügt werden um die Änderungen zu protokollieren
-   Bei komplizierten Funktionen / Klassen zusätzlich dazu Inline-Kommentare
