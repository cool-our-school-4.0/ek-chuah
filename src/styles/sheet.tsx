import { StyleSheet } from 'react-native';
import { Colors } from 'react-native-paper';

export const LocationStyles = StyleSheet.create({
    image: {
        width: '100%',
        height: '100%'
    },
    imageSmall: {
        height: 'auto',
        width: 'auto',
        resizeMode: 'contain',
        flexGrow: 1,
        flexShrink: 1,
        marginTop: 2,
        marginBottom: 2
    },
    blueHeadline: {
        color: Colors.blue900,
        marginTop: 8
    },
    description: {
        marginBottom: 8,
        color: Colors.blueGrey900
    },
    textInputsDefault: {
        backgroundColor: 'white',
        width: '90%',
        height: 33,
        color: Colors.blueGrey900
    },
    textInputsDescription: {
        backgroundColor: 'white',
        width: '90%',
        color: Colors.blueGrey900
    },
    textInputContainer: {
        padding: 10
    },
    tagsContainer: { flexBasis: '38%', paddingLeft: 10 },
    contentContainer: {
        flex: 1,
        backgroundColor: 'white',
        padding: 10
    }
});

export const ButtonStyle = StyleSheet.create({
    container: { backgroundColor: '#dee1f2', borderColor: Colors.white, borderWidth: 5 },
    textUnchecked: {
        fontSize: 19,
        color: Colors.blueGrey900
    },
    textChecked: {
        fontSize: 21,
        fontWeight: 'bold',
        color: Colors.blue900
    },
    default: {
        flexBasis: '95%',
        width: '95%',
        alignSelf: 'center',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    twoColumns: {
        width: '50%',
        flexGrow: 1
    },
    smallUnchecked: {
        flexBasis: '48%',
        marginTop: 11,
        backgroundColor: Colors.indigo200,
        borderColor: Colors.indigo200,
        borderWidth: 3
    },
    smallChecked: {
        flexBasis: '48%',
        marginTop: 11,
        backgroundColor: Colors.indigo200,
        borderColor: Colors.blue900,
        borderWidth: 3
    },
    mapButtonDefault: {
        backgroundColor: Colors.cyan600,
        position: 'absolute',
        bottom: 0
    }
});

export const ListStyle = StyleSheet.create({
    defaultList: {
        flex: 1,
        display: 'flex'
    },
    twoColumns: { flex: 1, display: 'flex', flexWrap: 'wrap', flexDirection: 'row' }
});

export const styles = StyleSheet.create({
    flexy: {
        flex: 1,
        backgroundColor: 'white'
    },
    flatlist: { height: 200, flexGrow: 0 },
    flatlistseperator: {
        height: 1,
        width: '86%',
        backgroundColor: Colors.cyan500,
        marginLeft: '14%'
    },
    tagsseperator: {
        height: 1,
        width: '100%',
        backgroundColor: Colors.cyan600
    },
    text: {
        color: Colors.blueGrey900
    },
    centeredText: {
        fontWeight: 'bold',
        color: Colors.blueGrey900,
        textAlign: 'center'
    }
});

export const TopBarStyle = StyleSheet.create({
    container: {
        backgroundColor: Colors.blue900,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: '8%'
    },
    text: {
        fontSize: 19,
        color: Colors.white
    }
});

export const MoveButtonStyle = StyleSheet.create({
    viewDefault: {
        position: 'absolute',
        bottom: '2%'
    },
    smaller: {
        height: 45,
        width: 45,
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewLeft: {
        left: '5%'
    },
    viewRight: {
        right: '5%'
    },
    default: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    }
});

export const LoadingComponentStyle = StyleSheet.create({
    container: { flex: 1, display: 'flex', alignItems: 'center' },
    successLabel: {
        textAlignVertical: 'center',
        textAlign: 'center',
        fontSize: 19,
        color: Colors.blueGrey900
    },
    containerItemsCentered: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    loadingElementOuterView: {
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center'
    },
    loadingElementInnerView: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%'
    },
    icon: { flex: 1, margin: 0, padding: 0, borderRadius: 0 },
    progress: { minWidth: 150, width: '80%' }
});

export const DatePickerStyle = StyleSheet.create({
    container: {
        width: '90%',
        display: 'flex',
        flex: 1,
        flexDirection: 'row',
        margin: '1%',
        alignItems: 'center',
        maxHeight: '7%',
        alignSelf: 'center'
    },
    textInput: {
        height: 40,
        color: Colors.blueGrey900
    },
    icon: {
        margin: 0
    },
    textView: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    text: {
        fontSize: 20,
        color: Colors.blueGrey900
    }
});

export const ModalStyle = StyleSheet.create({
    default: { flexBasis: '80%', width: '100%', alignSelf: 'center' }
});
