import { IGeocodingApiMapboxObjectFeature } from '../interfaces/imap-objects';
/**
 * Stores a label for map type and the style url fitting to the map type (dark/light/street/...)
 *
 * @author valentin_lechner
 * @interface
 * @typedef MapOptions
 * @property {string} label The label of an option
 * @property {any} data The data of an option
 */
export interface MapOptions {
    label: string;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    data: any;
}

/**
 * Interface that stores values for our map, do I need to ask for Location Permissions?
 *
 * @interface
 * @author valentin_lechner
 * @typedef MapState
 * @property {boolean} isAndroidPermissionGranted Has the permission to use the location of the user been granted?
 * @property {boolean} isFetchingAndroidPermission Is the permission currently in loading?
 * @property {any} styleUrl The Url for the style
 */
export interface MapState {
    isAndroidPermissionGranted: boolean;
    isFetchingAndroidPermission: boolean;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    styleUrl: any;
    userLocationActive: boolean;
    customLocation: IGeocodingApiMapboxObjectFeature;
}
