export interface CheckConditionType {
    isSatisfied: boolean;
    infoText: string;
}
