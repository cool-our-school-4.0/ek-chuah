import { IGeocodingApiMapboxObjectFeature } from '../../interfaces/imap-objects';
export const fallbackLocation: IGeocodingApiMapboxObjectFeature = {
    id: 'Default Location',
    type: 'Feature',
    // eslint-disable-next-line @typescript-eslint/camelcase
    place_type: ['place'],
    relevance: 1,
    properties: {},
    text: 'Freiburger Münster',
    // eslint-disable-next-line @typescript-eslint/camelcase
    place_name: 'Freiburger Münster',
    center: [7.851789, 47.9955581],
    geometry: {
        type: 'Point',
        coordinates: [7.851789, 47.9955581]
    },
    context: [
        {
            // eslint-disable-next-line @typescript-eslint/camelcase
            short_code: 'DE-BW',
            text: 'Baden-Württemberg',
            id: 'Default Region'
        }
    ]
};
