import { IGeocodingApiMapboxQueryType } from '../../../../interfaces/igeocoding-api';
import { defMapApiKeyHandler } from '../../../../handler/map-api-key-handler';
/**
 * Class handling the access Token Parameter for the Query
 *
 * @class
 * @implements {IGeocodingApiMapboxQueryType}
 */
export class QryParamAccToken implements IGeocodingApiMapboxQueryType {
    /** if the parameter is required. cant be changed */
    readonly required: boolean = true;
    /** the parameter key */
    public param: string = 'access_token';
    /** the parameter value */
    public varvalue: string = defMapApiKeyHandler.getMapApiKey();
}
/** The default, instantiated Api Token Handler */
export const defQryParamAccToken = new QryParamAccToken();
