import { IGeocodingApiMapboxQueryType } from '../../../../interfaces/igeocoding-api';
/**
 * Interface for Paramtype mapping
 *
 * @interface
 * @author valentin_lechner
 * @typedef {object} IQryParamType
 * @property {ParamType} type the paramtype
 * @property {boolean} active if the paramtype is currently selected
 */
export interface IQryParamType {
    type: ParamType;
    active: boolean;
}

/**
 * Enum for wrapping strings to a type
 *
 * @enum
 * @author valentin_lechner
 */
export enum ParamType {
    country = 'country',
    region = 'region',
    postcode = 'postcode',
    district = 'district',
    place = 'place',
    locality = 'locality',
    neighborhood = 'neighborhood',
    address = 'address'
}
/**
 * Class handling the Type Parameter for the Query
 *
 * @class
 * @implements {IGeocodingApiMapboxQueryType}
 * @author valentin_lechner
 */
export class QryParamType implements IGeocodingApiMapboxQueryType {
    readonly required: boolean = false;
    public availableTypes: string[] = [
        ParamType.address,
        ParamType.neighborhood,
        ParamType.locality,
        ParamType.place,
        ParamType.district,
        ParamType.postcode,
        ParamType.region,
        ParamType.country
    ];
    /** The currently active types */
    private typesActive: IQryParamType[] = this.setDefaultTypesActive();
    /** The param key */
    public param: string = 'types';
    /** The value of the param */
    public varvalue: string = this.getActiveTypes();
    /**
     * Set default types active adds all paramTypes in a list and sets them inactive
     *
     * @private
     * @function
     * @returns {IQryParamType[]} a list full of the paramtypes with the default setting telling us if they're enabled (now nothing enabled by default)
     * @author valentin_lechner
     */
    private setDefaultTypesActive(): IQryParamType[] {
        const tmp = new Array<IQryParamType>();
        for (const item in ParamType) {
            tmp.push({ type: item as ParamType, active: false });
        }
        return tmp;
    }
    /**
     * Sets passed types active
     *
     * @public
     * @param {ParamType[]} types The Types that should be searched for
     * @function
     * @returns {void}
     * @author valentin_lechner
     */
    public setTypesActive(types: ParamType[]): void {
        for (let i = 0; i < types.length; i++) {
            this.typesActive.forEach((j) => {
                if (j.type === types[i]) {
                    j.active = true;
                }
            });
        }
        this.varvalue = this.getActiveTypes();
    }
    /**
     * Returns the active types as value of the param
     *
     * @private
     * @function
     * @returns {string} the active types as value of the param (csv)
     * @author valentin_lechner
     */
    private getActiveTypes(): string {
        let ret: string = '';
        this.typesActive.forEach((i) => {
            if (i.active) {
                if (ret.trim() !== '') {
                    ret = `${ret},`;
                }
                ret = `${ret}${i.type}`;
            }
        });
        return ret;
    }
}
/** The default Handler for getting paramtypes as key-value for the param */
export const defQryParamType = new QryParamType();
