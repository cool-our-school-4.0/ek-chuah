import { IGeocodingApiApiUrlBuilder } from '../../../../interfaces/igeocoding-api';
import { defQryParamAccToken, QryParamAccToken } from './param-access-token';
import { defQryParamType, QryParamType, ParamType } from './param-type';
import { defQryParamProximity, QryParamProximity } from './param-proximity';

/**
 * @external Promise
 */

/**
 * The interface for setting the params that need to be passed to apiurlbuilder in a type
 *
 * @interface
 * @typedef {object} IApiUrlBuilder
 * @property {QryParamAccToken} accestoken the accesstokenhandler
 * @property {QryParamType} type the typehandler
 * @property {QryParamProximity} prox the proximity handler
 * @author valentin_lechner
 */
interface IApiUrlBuilder {
    accesstoken: QryParamAccToken;
    type: QryParamType;
    prox: QryParamProximity;
}
/**
 * Class for building a geocoding api url
 *
 * @class
 * @implements {IGeocodingApiApiUrlBuilder}
 * @author valentin_lechner
 */
export class GeocodingApiApiUrlBuilder implements IGeocodingApiApiUrlBuilder {
    /** the access token param handler */
    private accessTokenParam: QryParamAccToken;
    /** the type param handler */
    private typeParam: QryParamType;
    /** the proximity param handler */
    private proxParam: QryParamProximity;
    /** The api version */
    private readonly _apiVer: string = 'v5';
    /** The api service */
    private readonly _apiService: string = 'geocoding';
    /** The api Endpoint name */
    private readonly _apiEndpoint: string = 'mapbox.places';
    /** The api Baseurl */
    private readonly _baseUrl: string = `https://api.mapbox.com/${this._apiService}/${this._apiVer}/${this._apiEndpoint}/`;

    /**
     * Constructs the Api Url for Geocoding Requests using the classes datahandler
     *
     * @function
     * @returns {Promise<string>} The apiurl
     * @param {string} searchtext The string to search for
     * @param {boolean} useProximity Whether to add proximity as search param (if can get location)
     * @param {ParamType[]} types The active Types to search for
     * @async
     * @public
     * @author valentin_lechner
     */
    public async getApiUrl(searchtext: string, useProximity: boolean, types?: ParamType[]): Promise<string> {
        let retval = `${this._baseUrl}${searchtext}.json?`;

        if (types !== undefined) {
            this.typeParam.setTypesActive(types);
        }

        [this.typeParam, this.proxParam, this.accessTokenParam].forEach((paramhandler) => {
            const varvalue = paramhandler.varvalue;

            // only add if there is an actual value in the handler
            if (varvalue.trim() !== '') {
                if (!retval.endsWith('?') && !retval.endsWith('&')) {
                    retval = `${retval}&`;
                }

                if (paramhandler instanceof QryParamProximity) {
                    // need to check if we should use Proximity
                    if (useProximity) {
                        retval = `${retval}${paramhandler.param}=${paramhandler.varvalue}`;
                    }
                } else {
                    retval = `${retval}${paramhandler.param}=${paramhandler.varvalue}`;
                }
            }
        });
        return retval;
    }

    /**
     * Constructor
     *
     * @param {IApiUrlBuilder} props The Parameters for constructing ApiUrlBuilder
     * @author valentin_lechner
     * @class
     */
    constructor(props: IApiUrlBuilder) {
        this.accessTokenParam = props.accesstoken;
        this.proxParam = props.prox;
        this.typeParam = props.type;
    }
}
/** The default api Url Builder */
export const defGeocodingApiApiUrlBuilder = new GeocodingApiApiUrlBuilder({
    prox: defQryParamProximity,
    accesstoken: defQryParamAccToken,
    type: defQryParamType
});
