import { IMapLocationDataHandler } from '../../../../interfaces/imap';
import { IGeocodingApiMapboxQueryType } from '../../../../interfaces/igeocoding-api';
import { defMapLocationDataHandler } from '../../../../handler/map-location-data-handler';

/**
 * @external Promise
 */
/**
 * An Interface for passing the Params to the constructor of ParamProximity
 *
 * @interface
 * @typedef {object} IQryParamProximity
 * @property {IMapLocationDataHandler} locationDataHandler Handler for gettign the location
 * @author valentin_lechner
 */
interface IQryParamProximity {
    locationDataHandler: IMapLocationDataHandler;
}

/**
 * Class handling the Proximity Parameter for the Query
 *
 * @class
 * @implements {IGeocodingApiMapboxQueryType}
 * @author valentin_lechner
 */
export class QryParamProximity implements IGeocodingApiMapboxQueryType {
    /** Tells us if the parameter is required. cannot be modified (except for initialization) */
    readonly required: boolean = false;
    /** The location Data handler responsible for getting location */
    private locationDataHandler: IMapLocationDataHandler;
    /** The key value of the parameter */
    public param: string = 'proximity';
    /** The value part of the parameter */
    public varvalue: string = '';
    /**
     * Constructor
     *
     * @class
     * @param {IQryParamProximity} props passing the maplocationhandler
     * @author valentin_lechner
     */
    constructor(props: IQryParamProximity) {
        this.locationDataHandler = props.locationDataHandler;
        // for initializing the location, async process => have to call it as soon as we know we're going to need it
        this.getUserLocationString();
    }
    /**
     * Function setting the value part of the parameter by getting the location from the locationdatahandler
     *
     * @returns {Promise<void>} nothing, but sets the user location in the object
     * @private
     * @async
     * @author valentin_lechner
     */
    private async getUserLocationString(): Promise<void> {
        this.locationDataHandler.getUserLocationString().then((r) => (this.varvalue = r));
    }
}
/** Sets the default ParamProximity Handler for getting the key-value param for api requests */
export const defQryParamProximity = new QryParamProximity({ locationDataHandler: defMapLocationDataHandler });
