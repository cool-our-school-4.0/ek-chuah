import React from 'react';
import { GeocodingApiApiDataHandler } from '../../../handler/geocoding-api-apidatahandler';
import { IGeocodingApiMapboxObjectFeature } from '../../../interfaces/imap-objects';
import { IGeocodingApiApiDataHandler, IGeocodingApiMapbox } from '../../../interfaces/igeocoding-api';
import { ParamType } from './url-builder/param-type';
import { defGeocodingApiApiUrlBuilder, GeocodingApiApiUrlBuilder } from './url-builder/url-builder';
/**
 * @external Promise
 */

export class GeocodingApiMapbox extends React.Component<{ useProximity: boolean }> implements IGeocodingApiMapbox {
    /* Handler for receiving data from the api */
    private apiDataHandler: IGeocodingApiApiDataHandler = new GeocodingApiApiDataHandler();
    /** Handler for building the api url */
    private apiUrlBuilder: GeocodingApiApiUrlBuilder = defGeocodingApiApiUrlBuilder;
    /** whether to use proximity */
    public useProximity: boolean = true;
    /** saving currently loaded features */
    public currentFeatures: IGeocodingApiMapboxObjectFeature[] = new Array<IGeocodingApiMapboxObjectFeature>();
    /**
     * Constructor
     *
     * @param {{useProximity: boolean}} props whether to use proximity
     * @param props.useProximity
     * @author valentin_lechner
     */
    constructor(props: { useProximity: boolean }) {
        super(props);
        this.useProximity = props.useProximity;
    }

    /**
     * Builds API Url to call and orders to call it, returned value gets saved in local currentFeatures Object
     *
     * @param {string} name The name to query by
     * @param {ParamType[]} types the types to query by
     * @returns {Promise<void>} nothing, but sets the current features
     * @function
     * @public
     * @async
     * @author valentin_lechner
     */
    public async queryByName(name: string, types?: ParamType[]): Promise<void> {
        name = name.trim();
        if (name.length >= 3) {
            await this.apiUrlBuilder.getApiUrl(name, this.useProximity, types).then(async (result) => {
                await this.apiDataHandler.getFromUrl(result).then((r) => {
                    this.currentFeatures = r;
                });
            });
        } else if (name.length === 0) {
            this.currentFeatures.length = 0;
        }
    }
}
