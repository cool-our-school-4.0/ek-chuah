export class EUndefinedConfigException extends Error {
    constructor(name: string) {
        super(`${name} ist in der Konfigurationsdatei "./src/config.json" nicht definiert! Vorgang abgebrochen`);
        Object.setPrototypeOf(this, EUndefinedConfigException.prototype);
    }
}

export class EDatabaseLocationNotFound extends Error {
    constructor(id: number) {
        super(`Location ${id} konnte aus der Datenbank nicht geladen werden!`);
        Object.setPrototypeOf(this, EUndefinedConfigException.prototype);
    }
}
