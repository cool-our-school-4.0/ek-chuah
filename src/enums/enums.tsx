export enum ConfigEnum {
    backendApiUrl = 'backendApiUrl',
    backendAppId = 'backendAppId',
    backendApiKey = 'backendApiKey',
    mapApiKey = 'mapApiKey'
}
export enum TourDuration {
    short = 'ShortTour',
    medium = 'MediumTour',
    long = 'LongTour'
}

export enum SurpriseFactor {
    none = 'NoSurprise',
    small = 'SmallSurprise',
    full = 'FullSurprise'
}

export enum Moods {
    nature = 'Nature',
    coffee = 'Coffee',
    hiking = 'Hiking',
    handicap = 'Handicap',
    pubs = 'Pubs',
    party = 'Party',
    food = 'Food',
    sport = 'Sport',
    photo = 'Photospot',
    hidden = 'HiddenLocation',
    dancing = 'Dancing',
    viewpoint = 'Viewpoint',
    pizza = 'Pizza',
    culture = 'Culture',
    indoor = 'Indoor',
    sweet = 'Sweet',
    music = 'Music',
    bakery = 'Bakery',
    educating = 'Educating'
}

export const moodicons: Map<Moods, string> = new Map<Moods, string>();
moodicons.set(Moods.food, 'food');
moodicons.set(Moods.nature, 'nature');
moodicons.set(Moods.hiking, 'hiking');
moodicons.set(Moods.bakery, 'bread-slice');
moodicons.set(Moods.educating, 'school');
moodicons.set(Moods.hidden, 'map');
moodicons.set(Moods.music, 'music-circle');
moodicons.set(Moods.sweet, 'ice-cream');
moodicons.set(Moods.pizza, 'pizza');
moodicons.set(Moods.coffee, 'coffee');
moodicons.set(Moods.pubs, 'glass-mug');
moodicons.set(Moods.handicap, 'wheelchair-accessibility');
moodicons.set(Moods.party, 'glass-cocktail');
moodicons.set(Moods.sport, 'soccer');
moodicons.set(Moods.photo, 'camera');
moodicons.set(Moods.dancing, 'glass-cocktail');
moodicons.set(Moods.viewpoint, 'panorama');
moodicons.set(Moods.culture, 'bank');
moodicons.set(Moods.indoor, 'home');

export enum Routes {
    home = 'Home',
    timePeriod = 'TimePeriod',
    chooseLocation = 'ChooseLocation',
    datelocations = 'DateLocations',
    mood = 'Mood',
    newLocation = 'NewLocation',
    surpriseMeOrNot = 'SurpriseMeOrNot',
    computeRoute = 'ComputeRoute',
    routeTour = 'RouteTour',
    nav = 'Navigation',
    tour = 'Tour'
}

export enum Storagekeys {
    startLocation = 'StartLocation',
    date = 'Date',
    savedMoods = 'SavedMoods',
    chosenLocations = 'chosenLocations',
    surpriseFactor = 'SupriseFactor',
    tourDuration = 'TourDuration'
}

export enum LoadingType {
    route = 'Route',
    location = 'Location',
    dateLocation = 'DateLocation'
}
