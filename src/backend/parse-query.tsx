import Parse from 'parse/react-native';
import { IBackendQuery } from '../interfaces/ibackend-query';
import { ILocation } from '../interfaces/ilocations';
import { Moods } from '../enums/enums';
import { EDatabaseLocationNotFound } from '../exceptions/edata';

/**
 * Class implementing the backendquery interface for parse api
 *
 * @class
 * @author valentin_lechner
 * @implements {IBackendQuery}
 */
export class ParseQuery implements IBackendQuery {
    public async likeLocationById(id: number): Promise<void> {
        const location = await this.getLocationById(id);
        // even if we only get one item, we dont "know" it
        for (const loc of location) {
            const relevance = loc.get('relevance');

            if (loc !== undefined) {
                loc.set('relevance', relevance + 1);
                loc.save();
            } else {
                throw new EDatabaseLocationNotFound(id);
            }
        }
    }
    public async addNewLocation(loc: ILocation): Promise<void> {
        loc.locationid = (await this.getMaxLocationId()) + 1;
        const tmploc = new Parse.Object.extend('Locations');
        const location = new tmploc();
        location.set('locationid', loc.locationid);
        location.set('tags', loc.tags.join(','));
        location.set('description', loc.description);
        location.set('image', loc.image);
        location.set('latitude', loc.latitude);
        location.set('longitude', loc.longitude);
        location.set('locationname', loc.locationname);
        location.set('relevance', loc.relevance);
        location.save();
    }

    private async getMaxLocationId(): Promise<number> {
        return (
            new Parse.Query(Parse.Object.extend('Locations'))
                .descending('locationid')
                .first()
                // eslint-disable-next-line
                .then((result: any) => {
                    return result.get('locationid') as number;
                })
        );
    }

    public async getAllLocationsAsILocation(): Promise<ILocation[]> {
        const locations: ILocation[] = new Array<ILocation>();
        // eslint-disable-next-line
        return new Parse.Query(Parse.Object.extend('Locations')).find().then((results: any) => {
            for (const result of results) {
                const tmploc: ILocation = {
                    tags: (result.get('tags') as string).split(',').map((e) => {
                        return e as Moods;
                    }),
                    description: result.get('description'),
                    image: result.get('image'),
                    latitude: result.get('latitude'),
                    longitude: result.get('longitude'),
                    relevance: result.get('relevance'),
                    locationid: result.get('locationid'),
                    locationname: result.get('locationname')
                };
                locations.push(tmploc);
            }
            return locations;
        });
    }

    private async getLocationById(id: number): Promise<Parse.Object<Parse.Attributes>[]> {
        const results: Parse.Object<Parse.Attributes>[] = await new Parse.Query(Parse.Object.extend('Locations'))
            .equalTo('locationid', id)
            .find();
        if (results.length > 1) {
            console.error(`Location Id ${id} gibt es ${results.length}x in der Datenbank!`);
        } else if (results.length == 0) {
            throw new EDatabaseLocationNotFound(id);
        }
        return results;
    }

    public async getLocationByIdAsILocation(id: number): Promise<ILocation | void> {
        const results = await this.getLocationById(id);

        const location: ILocation = {
            tags: (results[0].get('tags') as string).split(',').map((e) => {
                return e as Moods;
            }),
            description: results[0].get('description'),
            image: results[0].get('image'),
            latitude: results[0].get('latitude'),
            longitude: results[0].get('longitude'),
            relevance: results[0].get('relevance'),
            locationid: results[0].get('locationid'),
            locationname: results[0].get('locationname')
        };
        return location;
    }
}
export const defParseQuery = new ParseQuery();
