import { IBackendHandler } from '../interfaces/ibackend-handler';
import { defParseBackendHandler } from '../handler/parse-backend-handler';
import { ILocation } from '../interfaces/ilocations';
import { IDatabase } from '../interfaces/idatabase';
/**
 * Class for accessing the database
 *
 * @implements {IDatabase}
 * @author valentin_lechner
 */
export class Database implements IDatabase {
    private _backendHandler: IBackendHandler;

    constructor(backendHandler: IBackendHandler) {
        this._backendHandler = backendHandler;
        this._backendHandler.backendConnection.initConnection();
    }
    public likeLocationById(id: number): Promise<void> {
        return this._backendHandler.backendQuery.likeLocationById(id);
    }

    /**
     * Loads all locations from database
     *
     * @returns {ILocation[]} all locations
     */
    public async addNewLocation(loc: ILocation): Promise<void> {
        return this._backendHandler.backendQuery.addNewLocation(loc);
    }
    /**
     * Loads all locations from database
     *
     * @returns {ILocation[]} all locations
     */
    public async getAllLocations(): Promise<ILocation[]> {
        return await this._backendHandler.backendQuery.getAllLocationsAsILocation();
    }

    /**
     * Loads first location matching id from database
     *
     * @param {number} id the id to search by
     * @returns {ILocation | void} the location matching, if any
     */
    public getLocationById(id: number): Promise<ILocation | void> {
        return this._backendHandler.backendQuery.getLocationByIdAsILocation(id);
    }
}
export const defParseDatabase = new Database(defParseBackendHandler);
