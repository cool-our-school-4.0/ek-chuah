import Parse from 'parse/react-native.js';
import { IBackendConnection } from '../interfaces/ibackend-connection';
import { defBackendUrlHandler, BackendUrlHandler } from '../handler/backend-url-handler';
import { defBackendApiKeyHandler, BackendApiKeyHandler } from '../handler/backend-api-key-handler';
import { defBackendAppIdHandler, BackendAppIdHandler } from '../handler/backend-app-id-handler';
const AsyncStorage = require('react-native').AsyncStorage;

/**
 * Class implementing backendconnection for parse  sdk
 *
 * @class
 * @implements {IBackendConnection}
 * @author valentin_lechner
 */
export class ParseConnector implements IBackendConnection {
    private _apiKeyHandler: BackendApiKeyHandler;
    private _appIdHandler: BackendAppIdHandler;
    private _urlHandler: BackendUrlHandler;

    /**
     * Initializes class data
     */
    constructor(
        handlerAppId: BackendAppIdHandler,
        handlerApiKey: BackendApiKeyHandler,
        handlerBackendUrl: BackendUrlHandler
    ) {
        this._appIdHandler = handlerAppId;
        this._apiKeyHandler = handlerApiKey;
        this._urlHandler = handlerBackendUrl;
    }

    /**
     * Initializes connection
     *
     * @function
     * @public
     * @author valentin_lechner
     */
    public initConnection(): void {
        Parse.initialize(this._appIdHandler.getAppId(), this._apiKeyHandler.getApiKey());
        Parse.serverURL = this._urlHandler.getBackendUrl();
        Parse.setAsyncStorage(AsyncStorage);
    }
}
export const defParseConnector = new ParseConnector(
    defBackendAppIdHandler,
    defBackendApiKeyHandler,
    defBackendUrlHandler
);
