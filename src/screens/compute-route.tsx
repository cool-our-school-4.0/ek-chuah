import React, { ReactElement } from 'react';
import { View, BackHandler, Text } from 'react-native';
import LoadingComponent from '../components/loading-component';
import TopBar from '../components/top-bar';
import { LoadingComponentStyle, styles } from '../styles/sheet';
import { Routes, LoadingType, Storagekeys } from '../enums/enums';
import { RoutingOptimization } from '../handler/routing-optimization-handler';
import AsyncStorage from '@react-native-community/async-storage';
import { ILocation } from '../interfaces/ilocations';
import { NavigationScreenProp, NavigationRoute } from 'react-navigation';
import { Button, Paragraph, Dialog, Portal } from 'react-native-paper';

type Props = {
    navigation: NavigationScreenProp<NavigationRoute>;
};

/**
 * Klasse zur Berechnung der Route
 *
 * @class
 * @author JanaRebmann
 * @augments {boolean} showCloseDialog
 * @augments {boolean} computingRouteFinished
 */
export default class ComputeRoute extends React.Component<Props> {
    state = { showCloseDialog: false, computingRouteFinished: false };
    constructor(props: { navigation: NavigationScreenProp<NavigationRoute> }) {
        super(props);
        this.onHardwareBackPress = this.onHardwareBackPress.bind(this);
    }

    componentDidMount(): void {
        this.computeRoute();
        BackHandler.addEventListener('hardwareBackPress', this.onHardwareBackPress);
    }

    componentWillUnmount(): void {
        BackHandler.removeEventListener('hardwareBackPress', this.onHardwareBackPress);
    }

    onHardwareBackPress(): boolean {
        this.setState({ showCloseDialog: true });
        return true;
    }

    /**
     * Render Method of ComputeRoute
     *
     * @function
     * @public
     * @returns {ReactElement} The screen which shows computing the route
     */
    render(): ReactElement {
        if (this.state.computingRouteFinished) {
            return (
                <View style={styles.flexy}>
                    <View style={LoadingComponentStyle.containerItemsCentered}>
                        <Text style={LoadingComponentStyle.successLabel}>Deine perfekte Route wurde ausgetüftelt!</Text>
                        <Portal>
                            <Dialog
                                visible={this.state.showCloseDialog}
                                onDismiss={(): void => {
                                    this.setState({ showCloseDialog: false });
                                }}
                            >
                                <Dialog.Title style={{ fontSize: 17 }}>
                                    Möchtest du zum Home Screen zurück?
                                </Dialog.Title>
                                <Dialog.Content>
                                    <Paragraph style={{ fontSize: 14 }}>
                                        Achtung! Wenn du zurück gehst, verlierst du deine aktuelle Route.
                                    </Paragraph>
                                </Dialog.Content>
                                <Dialog.Actions>
                                    <Button
                                        onPress={(): void => {
                                            this.setState({ showCloseDialog: false });
                                        }}
                                    >
                                        Nein noch nicht
                                    </Button>
                                    <Button
                                        onPress={(): void => {
                                            this.props.navigation.navigate(Routes.home);
                                        }}
                                    >
                                        Ja, bitte zurück!
                                    </Button>
                                </Dialog.Actions>
                            </Dialog>
                        </Portal>
                    </View>
                </View>
            );
        } else {
            return (
                <View style={styles.flexy}>
                    <TopBar text="Deine Route wird berechnet"></TopBar>
                    <View style={LoadingComponentStyle.containerItemsCentered}>
                        <LoadingComponent list={this.list} loadingType={LoadingType.route}></LoadingComponent>
                    </View>
                </View>
            );
        }
    }

    /**
     * computeRoute Method of ComputeRoute
     *
     * @function
     * @public
     */
    private async computeRoute(): Promise<void> {
        let tmplocations: ILocation[];

        const optimizedRoute = await AsyncStorage.getItem(Storagekeys.chosenLocations).then((r) => {
            if (r) {
                tmplocations = JSON.parse(r) as ILocation[];

                const routeOptimization = new RoutingOptimization();
                const retRoute: void | ILocation[] = routeOptimization.getShortestPath(tmplocations);
                return retRoute;
            }
            return;
        });

        let startLocationCoords: number[] = await AsyncStorage.getItem(Storagekeys.startLocation).then((r) => {
            if (r) {
                startLocationCoords = JSON.parse(r).geometry.coordinates as number[];
            }
            return startLocationCoords;
        });
        const startLoc: ILocation = {
            description: '',
            latitude: startLocationCoords[1],
            longitude: startLocationCoords[0],
            tags: [],
            image: '',
            locationid: -1,
            locationname: 'StartLocation',
            relevance: -1
        };

        this.setState({ computingRouteFinished: true });
        setTimeout(() => {
            // eslint-disable-next-line react/prop-types
            this.props.navigation.navigate(Routes.routeTour, { locs: optimizedRoute, startLoc: startLoc });
        }, 1000);
    }

    list: string[] = [
        'Straßen werden für dich gefegt...',
        'Wände werden für dich gestrichen...',
        'Blumen werden für dich gegossen...',
        'Mülleimer werden für dich geleert',
        'Autos werden für dich gewaschen',
        'Hauser werden für dich gebaut',
        'Rasen werden für dich gemäht',
        'Luft wird für dich gereinigt',
        'Bäume werden für dich gepflanzt'
    ];
}
