import React, { ReactElement } from 'react';
import { View, Text } from 'react-native';
import MoveForwardButton from '../components/move-forward-button';
import MoveBackwardButton from '../components/move-backward-button';
import { SelectionButtonList } from '../components/selection-button-list';
import { styles, DatePickerStyle } from '../styles/sheet';
import { CheckConditionType } from '../types/nav-button-types';
import TopBar from '../components/top-bar';
import DateTimePicker from '@react-native-community/datetimepicker';
import { TextInput, IconButton, Colors } from 'react-native-paper';
import moment from 'moment';
import { TouchableOpacity } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-community/async-storage';
import { TourDuration, Routes, Storagekeys } from '../enums/enums';

/**
 * Funktion der TimePeriod-View
 *
 * @function
 * @author valentin_lechner
 * @returns {ReactElement} The Screen for selecting the length of the trip the user wants to plan
 */
export default function TimePeriod(): ReactElement {
    const [date, setDate] = React.useState(new Date());
    const [showDateTimerPicker, setDateTimePicker] = React.useState(false);
    let selectButtonList: SelectionButtonList | null = null;

    /**
     * Enables the DatePicker
     *
     * @function
     * @returns {void} Emptyness
     */
    function onPressed(): void {
        setDateTimePicker(true);
    }

    /**
     * Gets Called if Date was picked
     *
     * @function
     * @param {Date|undefined} newDate the Date selected from the DatePicker
     * @returns {void} Emptyness
     */
    function onChange(newDate: Date | undefined): void {
        setDateTimePicker(false);
        const currentDate = newDate || date;
        setDate(currentDate);
    }
    return (
        <View style={styles.flexy}>
            <TopBar text="Wähle den Zeitraum deiner Tour"></TopBar>
            <View style={[DatePickerStyle.container]}>
                <TextInput
                    style={[styles.flexy, DatePickerStyle.textInput]}
                    onTouchStart={(): void => onPressed()}
                    mode="outlined"
                    underlineColor={Colors.cyan500}
                    selectionColor={Colors.cyan500}
                    editable={false}
                    render={(): Element => (
                        <TouchableOpacity style={[DatePickerStyle.textView]} onPress={(): void => onPressed()}>
                            <Text style={DatePickerStyle.text}>{moment(date).format('DD.MM.YYYY')}</Text>
                        </TouchableOpacity>
                    )}
                ></TextInput>
                <IconButton
                    icon="calendar"
                    onPress={(): void => onPressed()}
                    size={40}
                    style={DatePickerStyle.icon}
                ></IconButton>
            </View>
            <View>
                {showDateTimerPicker && (
                    <DateTimePicker
                        mode={'date'}
                        is24Hour={true}
                        value={date}
                        display={'calendar'}
                        onChange={(_, newDate): void => onChange(newDate)}
                    ></DateTimePicker>
                )}
            </View>
            <SelectionButtonList
                ref={(list): void => {
                    selectButtonList = list;
                }}
                default={{
                    buttons: [
                        { id: TourDuration.short, buttonText: 'Ich habe wenig Zeit (1-2h)' },
                        { id: TourDuration.medium, buttonText: 'Ich möchte viel erleben (3-4h)' },
                        { id: TourDuration.long, buttonText: 'Ich möchte viel erleben \n& habe Zeit (4h+)' }
                    ],
                    selectMultiple: false,
                    minAmountSelected: 1
                }}
                storageKey={Storagekeys.tourDuration}
            ></SelectionButtonList>
            <MoveForwardButton
                stashSave={(): void => {
                    AsyncStorage.setItem(Storagekeys.date, date.toLocaleString('de-DE'));
                    if (selectButtonList) {
                        selectButtonList.writeStorage();
                    }
                }}
                Destination={(): string => {
                    return Routes.mood;
                }}
                CheckCondition={(): CheckConditionType | undefined => {
                    return selectButtonList?.minAmountSelected();
                }}
            ></MoveForwardButton>
            <MoveBackwardButton></MoveBackwardButton>
        </View>
    );
}
