import React, { ReactElement } from 'react';
import { View, Text } from 'react-native';
import TopBar from '../components/top-bar';
import { styles, LoadingComponentStyle } from '../styles/sheet';
import { Routes, Storagekeys, SurpriseFactor, TourDuration, Moods, LoadingType } from '../enums/enums';
import AsyncStorage from '@react-native-community/async-storage';
import Location from '../components/location';
import LoadingComponent from '../components/loading-component';
import DateLocationHandler, { ReturnedLocations } from '../handler/date-location-handler';
import Swiper from 'react-native-deck-swiper';
import SubmitButton from '../components/submit-button';
import CancelButton from '../components/cancel-button';
import { withNavigation, NavigationScreenProp, NavigationRoute } from 'react-navigation';
import MoveForwardButton from '../components/move-forward-button';
import { CheckConditionType } from '../types/nav-button-types';
import { Button, Colors } from 'react-native-paper';
import { ILocation } from '../interfaces/ilocations';

/**
 * @external Promise
 */

/**
 * Klasse der DateLocations-View
 *
 * @author valentin_lechner
 * @returns {ReactElement} View for Choosing Locations to visit
 */
class DateLocations extends React.Component<{ navigation: NavigationScreenProp<NavigationRoute> }> {
    state: {
        ready: boolean;
        loadedStorage: boolean;
        startSelection: boolean;
        moveForwardManually: boolean;
        foundLocations: boolean;
        matchedNothing: boolean;
    } = {
        loadedStorage: false,
        ready: false,
        startSelection: false,
        moveForwardManually: false,
        foundLocations: false,
        matchedNothing: false
    };

    private chosenLocations: ILocation[] = [];
    private dateLocHandler: DateLocationHandler | null = null;
    private returnedLocations: ReturnedLocations = {
        amountUserSelect: -1,
        locations: [],
        min: -1,
        max: -1
    };
    private _swiperRef: React.RefObject<Swiper> = React.createRef();
    private swiperCurrentCardIndex: number = 0;

    /**
     * Function to read the stored data from AsyncStorage
     *
     * @function
     * @author valentin_lechner & jan_koerner
     * @returns {Promise<void>} An empty Promise
     */

    async componentDidMount(): Promise<void> {
        let startdate: Date = await AsyncStorage.getItem(Storagekeys.date).then((r) => {
            if (r) {
                startdate = new Date(r);
            }
            return startdate;
        });
        let startLocation: number[] = await AsyncStorage.getItem(Storagekeys.startLocation).then((r) => {
            if (r) {
                startLocation = JSON.parse(r).geometry.coordinates as number[];
            }
            return startLocation;
        });
        let surpriseFactor: SurpriseFactor = await AsyncStorage.getItem(Storagekeys.surpriseFactor).then((r) => {
            if (r) {
                surpriseFactor = JSON.parse(r)[0];
            }
            return surpriseFactor;
        });
        let tourDuration: TourDuration = await AsyncStorage.getItem(Storagekeys.tourDuration).then((r) => {
            if (r) {
                tourDuration = JSON.parse(r)[0];
            }
            return tourDuration;
        });
        let moods: Moods[] = await AsyncStorage.getItem(Storagekeys.savedMoods).then((r) => {
            if (r) {
                moods = JSON.parse(r);
            }
            return moods;
        });
        this.setState({ loadedStorage: true });

        await this.getAllLocations(startLocation, tourDuration, moods, surpriseFactor);
    }

    /**
     * Function to get all the Locations, for the tour, or for the user to select
     *
     * @function
     * @author valentin_lechner & jan_koerner
     * @param {number[]} startLocation lat and lon from the starting point
     * @param {TourDuration} tourDuration the duration
     * @param {Moods[]} moods the selected moods
     * @param {SurpriseFactor} surpriseFactor the surprise factor determines the amount of locations the user can select by himself
     * @returns {Promise<void>} An empty Promise
     */
    async getAllLocations(
        startLocation: number[],
        tourDuration: TourDuration,
        moods: Moods[],
        surpriseFactor: SurpriseFactor
    ): Promise<void> {
        if (!this.dateLocHandler) {
            this.dateLocHandler = new DateLocationHandler(tourDuration, surpriseFactor, moods, startLocation);
        }
        this.returnedLocations = await this.dateLocHandler.getLocations().catch((err: Error) => {
            return { amountUserSelect: -1, locations: [], max: -1, min: -1, message: err.message };
        });
        if (this.returnedLocations.locations.length > 0) {
            if (this.returnedLocations.amountUserSelect === 0) {
                this.chosenLocations = this.returnedLocations.locations;
                this.setState({ ready: true, foundLocations: true });
            } else {
                this.setState({ startSelection: true, foundLocations: true });
            }
        } else {
            this.setState({ startSelection: true });
        }

        return;
    }

    /**
     * Render Function
     *
     * @function
     * @author valentin_lechner & jan_koerner
     * @returns {ReactElement} loads locations and returns the view
     */
    render(): ReactElement {
        let reactElement: ReactElement;
        if (this.state.loadedStorage) {
            if (this.state.startSelection) {
                if (!this.state.matchedNothing) {
                    if (this.state.foundLocations) {
                        reactElement = this.foundLocations();
                    } else {
                        reactElement = this.foundZeroLocations();
                    }
                } else {
                    reactElement = this.lowerYourStandards();
                }
            } else {
                reactElement = (
                    <LoadingComponent
                        list={['Passende Locations werden gesucht']}
                        loadingType={LoadingType.dateLocation}
                    ></LoadingComponent>
                );
            }
        } else {
            reactElement = (
                <LoadingComponent
                    list={['Speicher wird ausgelesen']}
                    loadingType={LoadingType.dateLocation}
                ></LoadingComponent>
            );
        }
        return (
            <View style={styles.flexy}>
                <TopBar text="Date deine Locations"></TopBar>
                {reactElement}
            </View>
        );
    }

    /**
     * Function which displays the swipe Screen on the DateLocations Screen
     *
     * @function
     * @author jan_koerner
     * @returns {ReactElement} The Swipe Screen
     */
    private foundLocations(): ReactElement {
        return (
            <View style={styles.flexy}>
                <Swiper
                    ref={this._swiperRef}
                    useNativeDriver={true}
                    containerStyle={{
                        flex: 1,
                        display: 'flex'
                    }}
                    cardStyle={{ height: '100%', marginVertical: '0%' }}
                    cardVerticalMargin={0}
                    cards={this.returnedLocations.locations}
                    backgroundColor="transparent"
                    renderCard={(loc: ILocation): ReactElement => {
                        return <Location locationdbObject={loc}></Location>;
                    }}
                    onSwipedAll={(): void => {
                        this.setState({ ready: true });
                    }}
                    onSwipedRight={(index: number): void => {
                        this.chosenLocations.push(this.returnedLocations.locations[index]);
                        this.swiperCurrentCardIndex++;
                        this.checkIfEnoughSelected();
                    }}
                    onSwipedLeft={(): void => {
                        this.swiperCurrentCardIndex++;
                    }}
                    verticalSwipe={false}
                    overlayLabels={{
                        left: {
                            element: <Text>NOPE</Text>,
                            title: 'NOPE',
                            style: {
                                label: {
                                    backgroundColor: 'black',
                                    borderColor: 'black',
                                    color: 'white',
                                    borderWidth: 1
                                },
                                wrapper: {
                                    flexDirection: 'column',
                                    alignItems: 'flex-end',
                                    justifyContent: 'flex-start',
                                    marginTop: 30,
                                    marginLeft: -30
                                }
                            }
                        },
                        right: {
                            element: <Text>LIKE</Text> /* Optional */,
                            title: 'LIKE',
                            style: {
                                label: {
                                    backgroundColor: 'black',
                                    borderColor: 'black',
                                    color: 'white',
                                    borderWidth: 1
                                },
                                wrapper: {
                                    flexDirection: 'column',
                                    alignItems: 'flex-start',
                                    justifyContent: 'flex-start',
                                    marginTop: 30,
                                    marginLeft: 30
                                }
                            }
                        }
                    }}
                ></Swiper>
                <CancelButton
                    onPressFunction={(): void => {
                        // if all returned locations have been shown set state to ready
                        // if not increase the currentCardIndex and show the next location
                        if (this.swiperCurrentCardIndex === this.returnedLocations.locations.length - 1) {
                            this.setState({ ready: true });
                        } else {
                            this.swiperCurrentCardIndex++;
                            this._swiperRef.current.jumpToCardIndex(this.swiperCurrentCardIndex);
                        }
                    }}
                ></CancelButton>
                <SubmitButton
                    onPressFunction={(): void => {
                        this.chosenLocations.push(this.returnedLocations.locations[this.swiperCurrentCardIndex]);

                        // if all returned locations have been shown set state to ready
                        // if not increase the currentCardIndex and show the next location
                        if (this.swiperCurrentCardIndex === this.returnedLocations.locations.length - 1) {
                            this.setState({ ready: true });
                        } else {
                            this.swiperCurrentCardIndex++;
                            this._swiperRef.current.jumpToCardIndex(this.swiperCurrentCardIndex);
                            this.checkIfEnoughSelected();
                        }
                    }}
                ></SubmitButton>
                {this.state.moveForwardManually && (
                    <MoveForwardButton
                        higherPos={true}
                        Destination={(): string => {
                            return Routes.computeRoute;
                        }}
                        CheckCondition={(): CheckConditionType => {
                            return { infoText: '', isSatisfied: true };
                        }}
                        stashSave={async (): Promise<void> => {
                            await AsyncStorage.setItem(
                                Storagekeys.chosenLocations,
                                JSON.stringify(this.chosenLocations)
                            );
                        }}
                    ></MoveForwardButton>
                )}
            </View>
        );
    }

    /**
     * Function which displays a Screen when no Locations are found
     *
     * @function
     * @author jan_koerner
     * @returns {ReactElement} The Screen
     */

    private foundZeroLocations(): ReactElement {
        let reactElement: ReactElement;
        if (
            this.returnedLocations.max >= 0 &&
            this.returnedLocations.min == 0 &&
            this.returnedLocations.amountUserSelect < 0
        ) {
            reactElement = (
                <View style={styles.flexy}>
                    <View style={LoadingComponentStyle.containerItemsCentered}>
                        <Text style={LoadingComponentStyle.successLabel}>
                            {'In deiner Nähe sind noch keine \nLocations registriert. \n'}
                        </Text>
                        <Text style={LoadingComponentStyle.successLabel}>
                            {
                                'Du kannst jedoch selbst welche hinzufügen, wenn du im \n Home-Bildschirm auf \n"Neue Location anlegen" klickst!'
                            }
                        </Text>
                    </View>
                    <Button
                        color={Colors.cyan500}
                        style={{ marginBottom: '2%' }}
                        mode={'contained'}
                        icon={'home'}
                        onPress={(): void => {
                            this.props.navigation.navigate(Routes.home);
                        }}
                    >
                        Home
                    </Button>
                </View>
            );
        } else {
            reactElement = this.errorView();
        }
        return reactElement;
    }
    /**
     * Function which displays an Error-Screen when Error during the Location query occurs
     *
     * @function
     * @author jan_koerner
     * @returns {ReactElement} The Screen
     */
    private errorView(): ReactElement {
        return (
            <View style={styles.flexy}>
                <View style={LoadingComponentStyle.containerItemsCentered}>
                    <Text style={LoadingComponentStyle.successLabel}>Folgender Fehler ist aufgetreten:</Text>
                    <Text style={LoadingComponentStyle.successLabel}>{this.returnedLocations.message}</Text>
                    <Text style={LoadingComponentStyle.successLabel}>Bitte überprüfe deine Internetverbindung</Text>
                </View>
                <Button
                    color={Colors.cyan500}
                    style={{ marginBottom: '2%' }}
                    mode={'contained'}
                    icon={'home'}
                    onPress={(): void => {
                        this.props.navigation.navigate(Routes.home);
                    }}
                >
                    Home
                </Button>
            </View>
        );
    }

    /**
     * Function which shows a Screen if the user swiped all locations left (disliked all of them)
     *
     * @function
     * @author jan_koerner
     * @returns {ReactElement} The Screen
     */

    private lowerYourStandards(): ReactElement {
        return (
            <View style={styles.flexy}>
                <View style={LoadingComponentStyle.containerItemsCentered}>
                    <Text style={LoadingComponentStyle.successLabel}>Lower your Standards!</Text>
                </View>
                <Button
                    color={Colors.cyan500}
                    mode={'contained'}
                    style={{ marginBottom: '2%' }}
                    icon={'home'}
                    onPress={(): void => {
                        this.props.navigation.navigate(Routes.home);
                    }}
                >
                    Home
                </Button>
            </View>
        );
    }

    /**
     * Function to check, if the min Amount of Locs has been accepted, if yes Button to move forward is shown
     *
     * @function
     * @author jan_koerner
     * @returns {void} Emptyness
     */
    private checkIfEnoughSelected(): void {
        if (this.returnedLocations.min <= this.chosenLocations.length) {
            this.setState({ moveForwardManually: true });
        }
    }
    /**
     * Function to check, if we are ready to move on to the next screen if locations are selected
     * If not MatchedNothing Screen will appear
     *
     * @function
     * @author jan_koerner
     * @returns {void} Emptyness
     */
    async componentDidUpdate(): Promise<void> {
        if (this.state.ready) {
            if (this.chosenLocations.length > 0) {
                await AsyncStorage.setItem(Storagekeys.chosenLocations, JSON.stringify(this.chosenLocations));
                this.props.navigation.navigate(Routes.computeRoute);
            } else {
                if (!this.state.matchedNothing) {
                    this.setState({ matchedNothing: true });
                }
            }
        }
    }
}

export default withNavigation(DateLocations);
