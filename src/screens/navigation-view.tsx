import React, { ReactElement } from 'react';
import { View } from 'react-native';
import TopBar from '../components/top-bar';
import { styles } from '../styles/sheet';
import NavigationView from '../utils/native-modules';
import { ILocation } from '../interfaces/ilocations';
import { RouteProp } from '@react-navigation/native';
import { Routes } from '../enums/enums';
type RootStackParamList = {
    Navigation: { locs: ILocation[] | undefined; startLoc: ILocation };
};
/**
 * @external Props
 */
type TourRouteRouteProps = RouteProp<RootStackParamList, Routes.nav>;

type Props = {
    route: TourRouteRouteProps;
};
/**
 * Klasse der Map-View
 *
 * @function
 * @returns {ReactElement} The View routing the user
 * @author valentin_lechner
 * @param {Props} route routeProps
 */
export default function Navigation({ route }: Props): ReactElement {
    const locations = route.params.locs;
    if (locations) {
        locations.unshift(route.params.startLoc);
    }
    return (
        <View style={styles.flexy}>
            <TopBar text="Deine Map"></TopBar>
            <View style={{ display: 'flex', flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <NavigationView locs={JSON.stringify(locations)} style={{ flex: 1, width: '100%', height: '100%' }} />
            </View>
        </View>
    );
}
