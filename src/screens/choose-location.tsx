import AsyncStorage from '@react-native-community/async-storage';
import React, { Component, ReactElement } from 'react';
import { FlatList, View, EmitterSubscription, Keyboard } from 'react-native';
import { List, Searchbar } from 'react-native-paper';
import ShowMap from '../components/map';
import MoveBackwardButton from '../components/move-backward-button';
import MoveForwardButton from '../components/move-forward-button';
import TopBar from '../components/top-bar';
import { Routes, Storagekeys } from '../enums/enums';
import { styles } from '../styles/sheet';
import { GeocodingApiMapbox } from '../types/map/geocoding-api/geocoding-api-mapbox';
import { IGeocodingApiMapbox } from '../interfaces/igeocoding-api';
import { ParamType } from '../types/map/geocoding-api/url-builder/param-type';
import { IGeocodingApiMapboxObjectFeature } from '../interfaces/imap-objects';
import { CheckConditionType } from '../types/nav-button-types';

/** @external Promise */
/**
 * Funktion der ChooseLocation-View
 *
 * @author valentin_lechner
 * @class
 * @augments Component
 */
export default class ChooseLocation extends Component {
    /** The mutable state of the object containing features (from api req), the current place (in searchbar) and if the thing is currently loading (unused) */
    state: {
        features: IGeocodingApiMapboxObjectFeature[];
        place: string;
        showForwardButton: boolean;
    } = {
        features: [],
        place: '',
        showForwardButton: true
    };
    /** used to dynamically create a ref and access the state of the ShowMap Component in this view */
    private map: ShowMap | null = null;
    /** The api requester */
    private geocodingApi: IGeocodingApiMapbox = new GeocodingApiMapbox({ useProximity: false });

    private _keyboardShowEvent: EmitterSubscription | null = null;
    private _keyboardHideEvent: EmitterSubscription | null = null;

    /**
     * Event that triggers when component mounts
     *
     * @function
     * @returns {void} nothing
     * @public
     * @author valentin_lechner
     */
    public componentDidMount(): void {
        this.apiWrapper = this.apiWrapper.bind(this);
        this.onPress = this.onPress.bind(this);
        // Workaround, da wenn man weiter klickt und die Tastatur offen ist, dann werden button auf der nächsten Seite nicht richtig angezeigt
        this._keyboardShowEvent = Keyboard.addListener('keyboardDidShow', () => {
            this.setState({ showForwardButton: false });
        });
        this._keyboardHideEvent = Keyboard.addListener('keyboardDidHide', () => {
            this.setState({ showForwardButton: true });
        });
    }

    componentWillUnmount(): void {
        if (this._keyboardShowEvent !== null) {
            this._keyboardShowEvent.remove();
        }
        if (this._keyboardHideEvent !== null) {
            this._keyboardHideEvent.remove();
        }
    }

    /**
     * Function rendering a flatlist displaying some features to click on
     *
     * @function
     * @returns {ReactElement} View containing Flatlist with items
     * @private
     * @author valentin_lechner
     */
    private renderFlatlist(): ReactElement {
        return (
            <FlatList
                style={styles.flatlist}
                data={this.state.features}
                renderItem={({ item }): ReactElement => (
                    <List.Item
                        title={item.place_name}
                        description={item.text}
                        onPress={
                            /**
                             * @returns {void} nothing
                             */
                            (): void => {
                                return this.onPress(item);
                            }
                        }
                    />
                )}
                showsVerticalScrollIndicator={true}
                extraData={this.state}
                keyExtractor={(item): string => item.place_name}
                ItemSeparatorComponent={this.renderSeparator}
            />
        );
    }

    /**
     * Function rendering List.Item instead of a flatlist (when there are no items)
     *
     * @function
     * @returns {ReactElement} View containing a list.item that displays a helptext
     * @private
     * @author valentin_lechner
     */
    private renderWithoutFlatlist(): ReactElement {
        return (
            <List.Item
                title="Results show up here"
                description="Searches are currently processed by places, localities and districts"
            />
        );
    }

    /**
     * Function rendering Choose Location view
     *
     * @author jan_koerner
     * @function
     * @public
     * @returns {ReactElement} Die View Choose Location
     * Added SearchBar and FlatList for location suggestions
     */
    public render(): ReactElement {
        const { place } = this.state;
        return (
            <View style={styles.flexy}>
                <TopBar text="Wähle deine Stadt"></TopBar>
                <Searchbar
                    value={place}
                    placeholder="Wo beginnt deine Route?"
                    numberOfLines={1}
                    onChangeText={
                        /**
                         * @returns {Promise<void>} calls the api wrapper
                         */
                        (text: string): Promise<void> => this.apiWrapper(text)
                    }
                />
                {this.state.features.length > 0 && this.renderFlatlist()}
                <ShowMap
                    ref={(amap: ShowMap | null): void => {
                        this.map = amap;
                    }}
                />
                {this.state.showForwardButton && (
                    <MoveForwardButton
                        stashSave={(): void => {
                            AsyncStorage.setItem(
                                Storagekeys.startLocation,
                                JSON.stringify(this.map?.state.customLocation)
                            );
                        }}
                        Destination={(): string => {
                            return Routes.timePeriod;
                        }}
                        CheckCondition={(): CheckConditionType => {
                            return { infoText: '', isSatisfied: this.map?.state.customLocation !== null };
                        }}
                    />
                )}
                <MoveBackwardButton />
            </View>
        );
    }
    /**
     * Function calling the geocoding api. Contains local logic such as setting currently Loading and the search text
     *
     * @function
     * @author valentin_lechner
     * @returns {Promise<void>} nothing, but changes the state when getting data from apicall and re-renders the view
     * @param {string} text the search parameter
     * @async
     */
    private async apiWrapper(text: string): Promise<void> {
        this.setState({ place: text });
        await this.geocodingApi
            .queryByName(text, [ParamType.place, ParamType.locality, ParamType.district])
            .then(() => this.setState({ features: this.geocodingApi.currentFeatures }));
    }

    /**
     * Passes the selected element from the list to the ShowMap Component of the screen
     *
     * @function
     * @private
     * @author valentin_lechner, Jan Körner
     * @returns {void}
     * @param {IGeocodingApiMapboxObjectFeature} element The element returned from the onpress event in the list.item
     */
    private onPress(element: IGeocodingApiMapboxObjectFeature): void {
        if (this.map !== null) {
            this.map.setState({ customLocation: element, userLocationActive: false });
        }
    }

    /**
     * Renders a seperator between results of the listview
     *
     * @author valentin_lechner
     * @returns {ReactElement} a line as seperator
     */
    renderSeparator = (): ReactElement => {
        return <View style={styles.flatlistseperator} />;
    };
}
