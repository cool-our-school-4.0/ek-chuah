import React, { ReactElement } from 'react';
import { View } from 'react-native';
import { SelectionRouteButtonList } from '../components/selection-button-list';
import MoveForwardButton from '../components/move-forward-button';
import { styles } from '../styles/sheet';
import { CheckConditionType } from '../types/nav-button-types';
import TopBar from '../components/top-bar';
import { Routes } from '../enums/enums';
import { requestLocationPermission } from '../handler/permissions-handler';

/**
 * Funktion der Home-View
 *
 * @function
 * @returns {ReactElement} Home View
 * @author valentin_lechner
 */
export default function Home(): ReactElement {
    let selectButtonList: SelectionRouteButtonList | null = null;
    requestLocationPermission();
    return (
        <View style={styles.flexy}>
            <TopBar text="Was willst du tun?"></TopBar>
            <SelectionRouteButtonList
                ref={(list): void => {
                    selectButtonList = list;
                }}
                buttons={[
                    {
                        button: { buttonText: 'Neue Location anlegen', id: 'NewLocation' },
                        routeName: Routes.newLocation
                    },
                    { button: { buttonText: 'Tour planen', id: 'PlanTour' }, routeName: Routes.chooseLocation }
                ]}
                selectMultiple={false}
                minAmountSelected={1}
            ></SelectionRouteButtonList>
            <MoveForwardButton
                Destination={(): string | undefined => {
                    return selectButtonList?.getCurrentRoute();
                }}
                CheckCondition={(): CheckConditionType | undefined => {
                    return selectButtonList?.minAmountSelected();
                }}
            ></MoveForwardButton>
        </View>
    );
}
