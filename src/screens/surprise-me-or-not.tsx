import React, { ReactElement } from 'react';
import { View } from 'react-native';
import { styles } from '../styles/sheet';
import { SelectionButtonList } from '../components/selection-button-list';
import MoveForwardButton from '../components/move-forward-button';
import MoveBackwardButton from '../components/move-backward-button';
import { CheckConditionType } from '../types/nav-button-types';
import TopBar from '../components/top-bar';
import { SurpriseFactor, Routes, Storagekeys } from '../enums/enums';
/**
 * Funktion der SupriseMeOrNot-View
 *
 * @function
 * @author valentin_lechner
 * @returns {ReactElement} The Screen for selecting how many Surprises the user wants in his tour
 */
export default function SurpriseMeOrNot(): ReactElement {
    let selectButtonList: SelectionButtonList | null = null;
    return (
        <View style={styles.flexy}>
            <TopBar text="Wie soll deine Tour geplant werden?"></TopBar>

            <SelectionButtonList
                ref={(list): void => {
                    selectButtonList = list;
                }}
                default={{
                    buttons: [
                        { id: SurpriseFactor.none, buttonText: 'Ich kann schon selbst entscheiden!' },
                        { id: SurpriseFactor.small, buttonText: 'Ich möchte ein bisschen \nmitbestimmen' },
                        { id: SurpriseFactor.full, buttonText: 'Überrascht mich :)' }
                    ],
                    selectMultiple: false,
                    minAmountSelected: 1
                }}
                storageKey={Storagekeys.surpriseFactor}
            ></SelectionButtonList>
            <MoveForwardButton
                stashSave={(): void => {
                    if (selectButtonList) {
                        selectButtonList.writeStorage();
                    }
                }}
                Destination={(): string => {
                    return Routes.datelocations;
                }}
                CheckCondition={(): CheckConditionType | undefined => {
                    return selectButtonList?.minAmountSelected();
                }}
            ></MoveForwardButton>
            <MoveBackwardButton></MoveBackwardButton>
        </View>
    );
}
