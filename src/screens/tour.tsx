import React, { ReactElement } from 'react';
import { View, ScrollView, Image, ImageSourcePropType, FlatList } from 'react-native';
import { Text, FAB, Colors } from 'react-native-paper';
import TopBar from '../components/top-bar';
import { styles, LocationStyles, MoveButtonStyle } from '../styles/sheet';
import { ILocation } from '../interfaces/ilocations';
import { RouteProp } from '@react-navigation/native';
import { Routes } from '../enums/enums';
import images from '../data/locations/images';
import LikeButton from '../components/like-button';
import Location from '../components/location';
import MoveBackwardButton from '../components/move-backward-button';

type TourRouteRouteProps = RouteProp<Record<string, ILocation>, Routes.tour>;

type Props = {
    route: TourRouteRouteProps;
};

/**
 * Class returning the 'Tour' View
 *
 * @author valentin_lechner
 * @class
 */
export default class Tour extends React.Component<{}, { screenState: number }> {
    route: TourRouteRouteProps;
    screenStates = { showList: 0, showDetails: 1 };
    location: ILocation = {
        locationid: -1,
        tags: [],
        description: '',
        locationname: '',
        latitude: 0,
        longitude: 0,
        relevance: 0,
        image: 'crocodile'
    };

    constructor({ route }: Props) {
        super({ route });
        this.route = route;
        this.state = { screenState: this.screenStates.showList };
    }

    /**
     * Render Method of Tour
     *
     * @function
     * @public
     * @returns {ReactElement} The screen depending on the screen state
     */
    render(): ReactElement {
        if (this.state.screenState == this.screenStates.showList) {
            return this.returnListView();
        } else if (this.state.screenState == this.screenStates.showDetails) {
            return this.returnDetailView();
        } else {
            return (
                <View style={styles.flexy}>
                    <TopBar text="Neue Location anlegen"></TopBar>
                    <Text>Fehler: aktueller State nicht gesetzt</Text>
                    <MoveBackwardButton></MoveBackwardButton>
                </View>
            );
        }
    }

    /**
     * returnListView Method of Tour
     *
     * @function
     * @public
     * @returns {ReactElement} The screen which shows the list of locations
     */
    returnListView(): ReactElement {
        return (
            <View style={styles.flexy}>
                <TopBar text="Deine Tour"></TopBar>
                <ScrollView persistentScrollbar={true}>
                    <FlatList
                        style={{ flex: 1, marginRight: 13 }}
                        keyExtractor={(item: ILocation): string => item.locationid.toString()}
                        data={Object.values(this.route.params)}
                        ItemSeparatorComponent={(): ReactElement => {
                            return <View style={[styles.tagsseperator, { marginLeft: 10 }]} />;
                        }}
                        renderItem={({ item }): ReactElement => {
                            return (
                                <View style={{ maxHeight: 170, flexDirection: 'row' }}>
                                    <View style={{ flexBasis: '27%', flexDirection: 'column' }}>
                                        {item.image ? (
                                            <Image
                                                source={images[item.image] as ImageSourcePropType}
                                                style={LocationStyles.imageSmall}
                                            ></Image>
                                        ) : (
                                            <Image
                                                source={require('../data/locations/images/404_not_found_4x.png')}
                                                style={LocationStyles.imageSmall}
                                            ></Image>
                                        )}
                                    </View>

                                    <View
                                        style={{
                                            flexDirection: 'column',
                                            flexBasis: '73%',
                                            justifyContent: 'center',
                                            paddingLeft: 10,
                                            paddingRight: 10,
                                            paddingTop: 10,
                                            paddingBottom: 10
                                        }}
                                    >
                                        <Text style={{ fontSize: 16 }}>{item.locationname}</Text>
                                        <Text numberOfLines={3}>{item.description}</Text>
                                        <View style={{ height: 50, marginTop: 8 }}>
                                            <LikeButton locationId={item.locationid}></LikeButton>
                                            <View style={[MoveButtonStyle.viewDefault, { right: '15%' }]}>
                                                <FAB
                                                    icon={'chevron-down'}
                                                    style={[MoveButtonStyle.viewDefault, MoveButtonStyle.smaller]}
                                                    onPress={(): void => {
                                                        this.location = {
                                                            locationid: item.locationid,
                                                            tags: item.tags,
                                                            description: item.description,
                                                            locationname: item.locationname,
                                                            latitude: item.latitude,
                                                            longitude: item.longitude,
                                                            relevance: item.relevance,
                                                            image: item.image
                                                        };
                                                        this.setState({ screenState: this.screenStates.showDetails });
                                                    }}
                                                ></FAB>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            );
                        }}
                    ></FlatList>
                </ScrollView>
            </View>
        );
    }

    /**
     * returnDetailView Method of Tour
     *
     * @function
     * @public
     * @returns {ReactElement} The screen which shows the details of one locations
     */
    returnDetailView(): ReactElement {
        return (
            <View style={styles.flexy}>
                <TopBar text="Detailansicht"></TopBar>
                <View style={[styles.flexy, { paddingLeft: 10, paddingRight: 10, backgroundColor: Colors.blue900 }]}>
                    <Location locationdbObject={this.location}></Location>
                    <View style={[MoveButtonStyle.viewDefault, { right: '18%' }]}>
                        <FAB
                            icon={'chevron-up'}
                            style={[MoveButtonStyle.viewDefault, MoveButtonStyle.smaller]}
                            onPress={(): void => {
                                this.setState({ screenState: this.screenStates.showList });
                            }}
                        ></FAB>
                    </View>
                </View>
            </View>
        );
    }
}
