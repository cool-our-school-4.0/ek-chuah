import React, { ReactElement } from 'react';
import { View } from 'react-native';
import { SelectionButtonList } from '../components/selection-button-list';
import MoveForwardButton from '../components/move-forward-button';
import MoveBackwardButton from '../components/move-backward-button';
import { styles } from '../styles/sheet';
import { CheckConditionType } from '../types/nav-button-types';
import TopBar from '../components/top-bar';
import { Moods, Routes, Storagekeys } from '../enums/enums';

/**
 * Funktion der Mood-View
 *
 * @function
 * @returns {ReactElement} Returns the View for selecting Moods to plan the tour with
 * @author valentin_lechner
 */
export default function Mood(): ReactElement {
    let selectButtonList: SelectionButtonList | null = null;
    return (
        <View style={styles.flexy}>
            <TopBar text="Was darf auf keinen Fall fehlen?"></TopBar>
            <SelectionButtonList
                ref={(list): void => {
                    selectButtonList = list;
                }}
                default={{
                    buttons: [
                        { id: Moods.nature, buttonText: 'Natur' },
                        { id: Moods.coffee, buttonText: 'Kaffee' },
                        { id: Moods.hiking, buttonText: 'Wandern' },
                        { id: Moods.pubs, buttonText: 'Pubs' },
                        { id: Moods.party, buttonText: 'Party' },
                        { id: Moods.food, buttonText: 'Essen' },
                        { id: Moods.sport, buttonText: 'Sport' },
                        { id: Moods.photo, buttonText: 'Fotospot' },
                        { id: Moods.hidden, buttonText: 'Hidden Location' },
                        { id: Moods.dancing, buttonText: 'Dancing' },
                        { id: Moods.viewpoint, buttonText: 'Aussichtspunkt' },
                        { id: Moods.pizza, buttonText: 'Pizza' },
                        { id: Moods.culture, buttonText: 'Kultur' },
                        { id: Moods.indoor, buttonText: 'Indoor' },
                        { id: Moods.sweet, buttonText: 'Süßes' },
                        { id: Moods.music, buttonText: 'Musik' },
                        { id: Moods.bakery, buttonText: 'Backwaren' },
                        { id: Moods.educating, buttonText: 'Lehrreiches' },
                        { id: Moods.handicap, buttonText: 'Behindertengerecht' }
                    ],
                    selectMultiple: true,
                    minAmountSelected: 0,
                    twoColumns: true
                }}
                storageKey={Storagekeys.savedMoods}
            ></SelectionButtonList>
            <MoveForwardButton
                stashSave={(): void => {
                    if (selectButtonList) {
                        selectButtonList.writeStorage();
                    }
                }}
                Destination={(): string => {
                    return Routes.surpriseMeOrNot;
                }}
                CheckCondition={(): CheckConditionType | undefined => {
                    return selectButtonList?.minAmountSelected();
                }}
            ></MoveForwardButton>
            <MoveBackwardButton></MoveBackwardButton>
        </View>
    );
}
