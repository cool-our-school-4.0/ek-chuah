import React, { ReactElement } from 'react';
import Navigation from './navigation-view';
import Tour from './tour';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import { Colors } from 'react-native-paper';
import { Routes } from '../enums/enums';
import { ILocation } from '../interfaces/ilocations';
import { RouteProp } from '@react-navigation/native';

type RootStackParamList = {
    RouteTour: { locs: ILocation[] | undefined; startLoc: ILocation };
};

type TourRouteRouteProps = RouteProp<RootStackParamList, Routes.routeTour>;

type Props = {
    route: TourRouteRouteProps;
};

/**
 * Funktion die den Tab navigator zurückgibt
 *
 * @author jan_koerner
 * @returns {ReactElement} Ein Element das zwei Tabs Route/Tour in einer View darstellt
 * @function
 */
// eslint-disable-next-line react/prop-types
export default function TourRouteTabNavigator({ route }: Props): ReactElement {
    /**
     * Funktion die den TabNavigator implementiert
     *
     * @author jan_koerner
     */
    const Tab = createMaterialBottomTabNavigator();
    return (
        <Tab.Navigator barStyle={{ backgroundColor: Colors.blue900 }}>
            <Tab.Screen
                initialParams={route.params}
                name={Routes.nav}
                component={Navigation}
                options={{ tabBarIcon: 'map' }}
            />
            <Tab.Screen
                initialParams={route.params.locs}
                name={Routes.tour}
                component={Tour}
                options={{ tabBarIcon: 'view-list' }}
            />
        </Tab.Navigator>
    );
}
