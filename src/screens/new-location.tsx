import React, { ReactElement } from 'react';
import { View, FlatList, ScrollView } from 'react-native';
import MoveBackwardButton from '../components/move-backward-button';
import {
    Text,
    Subheading,
    TextInput,
    Colors,
    FAB,
    Button,
    Checkbox,
    Modal,
    Portal,
    IconButton
} from 'react-native-paper';
import Snackbar from 'react-native-snackbar';
import LoadingComponent from '../components/loading-component';
import MoveForwardButton from '../components/move-forward-button';
import { CheckConditionType } from '../types/nav-button-types';
import {
    LoadingComponentStyle,
    styles,
    LocationStyles,
    MoveButtonStyle,
    ButtonStyle,
    ModalStyle
} from '../styles/sheet';
import TopBar from '../components/top-bar';
import { Routes, LoadingType, Moods, moodicons } from '../enums/enums';
import Location from '../components/location';
import SubmitButton from '../components/submit-button';
import CancelButton from '../components/cancel-button';
import { ILocation } from '../interfaces/ilocations';
import { defParseDatabase as Database } from '../backend/database';
import { defMapLocationDataHandler } from '../handler/map-location-data-handler';
import { IMapCoords } from '../interfaces/imap';
import ShowMap from '../components/map';
/**
 * @external Map
 */
/**
 * Klasse der NewLocation-View
 *
 * @class
 * @author JanaRebmann
 */
export default class NewLocation extends React.Component<
    {},
    {
        screenState: number;
        useCurrentPosition: boolean;
        showHelperText: boolean;
        checkedTags: Map<Moods, boolean>;
        choseFromMap: boolean;
        mapPopup: boolean;
    }
> {
    list: string[] = ['Bits werden übertragen...'];
    screenStates = { addingScreen: 0, preview: 1, isSavingScreen: 2, savedScreen: 3 };
    state = {
        screenState: this.screenStates.addingScreen,
        useCurrentPosition: false,
        showHelperText: false,
        checkedTags: new Map<Moods, boolean>(),
        choseFromMap: false,
        mapPopup: false
    };
    private _map: ShowMap | null = null;

    private _newLocation: ILocation = {
        locationid: -1,
        tags: [],
        description: '',
        locationname: '',
        latitude: 0,
        longitude: 0,
        relevance: 0,
        image: ''
    };

    constructor(props: {
        screenState: number;
        useCurrentPosition: boolean;
        showHelperText: boolean;
        checkedTags: Map<Moods, boolean>;
    }) {
        super(props);
        for (const mood of Object.values(Moods)) {
            this.state.checkedTags.set(mood, false);
        }
    }

    /**
     * Render Method of NewLocation
     *
     * @function
     * @public
     * @returns {ReactElement} The screen which shows saving the location
     */
    render(): ReactElement {
        switch (this.state.screenState) {
            case this.screenStates.addingScreen:
                return this.returnAddingView();
            case this.screenStates.preview:
                return this.returnPreviewView();
            case this.screenStates.isSavingScreen:
                return this.returnIsSavingView();
            case this.screenStates.savedScreen:
                return this.returnSavedView();
            default:
                return (
                    <View style={styles.flexy}>
                        <TopBar text="Neue Location anlegen"></TopBar>
                        <Text>Fehler: aktueller State nicht gesetzt</Text>
                        <MoveBackwardButton></MoveBackwardButton>
                    </View>
                );
        }
    }

    /**
     * saveLocation Method of NewLocation
     *
     * @function
     * @public
     */
    private async saveLocation(): Promise<void> {
        if (this.state.useCurrentPosition) {
            const tmp: IMapCoords = await defMapLocationDataHandler.getUserLocation();
            this._newLocation.latitude = tmp.lat;
            this._newLocation.longitude = tmp.lon;
        }

        Database.addNewLocation(this._newLocation).then(() => {
            this.setState({ screenState: this.screenStates.savedScreen });
        });
    }

    /**
     * returnAddingScreen Method of NewLocation
     *
     * @function
     * @public
     * @returns {ReactElement} the screen to add a location
     */
    private returnAddingView(): ReactElement {
        return (
            <View style={styles.flexy}>
                <TopBar text="Neue Location anlegen"></TopBar>
                {this.mapPopUp()}
                <View style={{ flexBasis: '80%' }}>
                    {this.insertLocationName()}
                    {this.insertDesciption()}
                    {this.insertTags()}
                    {this.insertCoordinates()}
                </View>
                <View style={[MoveButtonStyle.viewDefault, MoveButtonStyle.viewRight]}>
                    <FAB
                        icon={'arrow-right-bold'}
                        style={MoveButtonStyle.default}
                        onPress={(): void => {
                            this._newLocation.tags = [];
                            this.state.checkedTags.forEach((checked, mood) => {
                                if (checked) {
                                    this._newLocation.tags.push(mood);
                                }
                            });
                            if (
                                this._newLocation.locationname &&
                                this._newLocation.description &&
                                this._newLocation.tags.length > 0 &&
                                (this.state.useCurrentPosition || this.state.choseFromMap)
                            ) {
                                this.setState({ screenState: this.screenStates.preview });
                            } else {
                                Snackbar.show({
                                    text: 'Bitte fülle alle Felder aus',
                                    duration: Snackbar.LENGTH_SHORT,
                                    backgroundColor: Colors.blue900,
                                    textColor: Colors.white
                                });
                            }
                        }}
                    ></FAB>
                </View>
                <MoveBackwardButton></MoveBackwardButton>
            </View>
        );
    }
    /**
     * mapPopUp Method of NewLocation
     *
     * @function
     * @public
     * @returns {ReactElement} the screen to select location on a map
     */
    private mapPopUp(): ReactElement {
        return (
            <Portal>
                <Modal
                    visible={this.state.mapPopup}
                    contentContainerStyle={ModalStyle.default}
                    onDismiss={(): void => {
                        this.setState({ mapPopup: false });
                    }}
                >
                    <View style={styles.flexy}>
                        <ShowMap
                            ref={(amap: ShowMap | null): void => {
                                this._map = amap;
                            }}
                        ></ShowMap>
                        <FAB
                            icon={'content-save'}
                            style={[ButtonStyle.mapButtonDefault, { alignSelf: 'flex-end' }]}
                            onPress={(): void => {
                                if (this._map) {
                                    const customLocation = this._map.state.customLocation;
                                    this._newLocation.latitude = customLocation.geometry.coordinates[0];
                                    this._newLocation.longitude = customLocation.geometry.coordinates[1];
                                    this.setState({
                                        choseFromMap: true,
                                        useCurrentPosition: false,
                                        mapPopup: false
                                    });
                                }
                            }}
                        ></FAB>
                        <FAB
                            icon={'close'}
                            style={[ButtonStyle.mapButtonDefault, { alignSelf: 'flex-start' }]}
                            onPress={(): void => {
                                this.setState({ mapPopup: false });
                            }}
                        ></FAB>
                    </View>
                </Modal>
            </Portal>
        );
    }

    /**
     * insertLocationName Method of NewLocation
     *
     * @function
     * @public
     * @returns {ReactElement} Location Name input
     */
    private insertLocationName(): ReactElement {
        return (
            <View style={LocationStyles.textInputContainer}>
                <Subheading style={styles.centeredText}>Name des Ortes</Subheading>
                <TextInput
                    mode={'flat'}
                    underlineColor={Colors.cyan600}
                    style={LocationStyles.textInputsDefault}
                    defaultValue={this._newLocation.locationname}
                    onChangeText={(text): void => {
                        this._newLocation.locationname = text;
                    }}
                />
            </View>
        );
    }
    /**
     * insertDesciption Method of NewLocation
     *
     * @function
     * @public
     * @returns {ReactElement} Description input
     */
    private insertDesciption(): ReactElement {
        return (
            <View style={LocationStyles.textInputContainer}>
                <Subheading style={styles.centeredText}>Beschreibung</Subheading>
                <TextInput
                    mode={'flat'}
                    underlineColor={Colors.cyan600}
                    multiline
                    maxLength={200}
                    style={LocationStyles.textInputsDescription}
                    defaultValue={this._newLocation.description}
                    onChangeText={(text): void => {
                        this._newLocation.description = text;
                    }}
                />
            </View>
        );
    }
    /**
     * insertTags Method of NewLocation
     *
     * @function
     * @public
     * @returns {ReactElement} tags input
     */
    private insertTags(): ReactElement {
        return (
            <View style={(LocationStyles.textInputContainer, LocationStyles.tagsContainer)}>
                <Subheading style={styles.centeredText}>Tags</Subheading>
                <ScrollView persistentScrollbar={true}>
                    <FlatList
                        style={{ flex: 1 }}
                        keyExtractor={(item): string => item.toString()}
                        data={Object.values(Moods)}
                        ItemSeparatorComponent={(): ReactElement => {
                            return <View style={[styles.tagsseperator, { width: '90%' }]} />;
                        }}
                        renderItem={({ item }): ReactElement => {
                            const iconName = moodicons.get(item) || 'map';
                            return (
                                <View style={{ flexDirection: 'row', flexBasis: '100%' }}>
                                    <IconButton icon={iconName} />
                                    <Checkbox.Item
                                        theme={{ colors: { primary: 'black' } }}
                                        labelStyle={{ flexBasis: '70%' }}
                                        label={item}
                                        status={this.state.checkedTags.get(item) ? 'checked' : 'unchecked'}
                                        color={Colors.blue900}
                                        onPress={(): void => {
                                            this.pressedCheckbox(item, this.state.checkedTags);
                                        }}
                                    />
                                </View>
                            );
                        }}
                    ></FlatList>
                </ScrollView>
            </View>
        );
    }

    /**
     * pressed Method of NewLocation
     *
     * @function
     * @public
     * @returns {void} Emptyness
     * @param {Moods} item the pressed mood
     * @param {Map<Moods, boolean>} tags the current checkedTags
     */
    private pressedCheckbox(item: Moods, tags: Map<Moods, boolean>): void {
        const currentValue = this.state.checkedTags.get(item) || false;
        tags.set(item, !currentValue);
        this.setState({ checkedTags: tags });
    }

    /**
     * insertDesciption Method of NewLocation
     *
     * @function
     * @public
     * @returns {ReactElement} Description input
     */
    private insertCoordinates(): ReactElement {
        return (
            <View style={LocationStyles.textInputContainer}>
                <Subheading style={styles.centeredText}>Koordinaten</Subheading>
                <View style={{ flexDirection: 'row', justifyContent: 'space-evenly' }}>
                    <Button
                        icon={this.state.useCurrentPosition ? 'check-bold' : 'map-marker'}
                        mode="contained"
                        style={this.state.useCurrentPosition ? ButtonStyle.smallChecked : ButtonStyle.smallUnchecked}
                        onPress={(): void => {
                            const currentState: boolean = this.state.useCurrentPosition;
                            this.setState({ useCurrentPosition: !currentState });
                            if (!currentState) {
                                this.setState({
                                    choseFromMap: false
                                });
                            }
                        }}
                    >
                        Standort
                    </Button>
                    <Button
                        icon={this.state.choseFromMap ? 'check-bold' : 'map'}
                        mode="contained"
                        style={this.state.choseFromMap ? ButtonStyle.smallChecked : ButtonStyle.smallUnchecked}
                        onPress={(): void => {
                            this.setState({ mapPopup: true });
                        }}
                    >
                        Map
                    </Button>
                </View>
            </View>
        );
    }

    /**
     * returnPreviewView Method of NewLocation
     *
     * @function
     * @public
     * @returns {ReactElement} the screen with a preview of the location
     */
    private returnPreviewView(): ReactElement {
        return (
            <View style={styles.flexy}>
                <TopBar text="Vorschau deiner neuen Location"></TopBar>
                <Location locationdbObject={this._newLocation}></Location>
                <CancelButton onPressFunction={this.goToAddingView.bind(this)}></CancelButton>
                <SubmitButton onPressFunction={this.goToIsSavingView.bind(this)}></SubmitButton>
            </View>
        );
    }

    /**
     * returnSavingView Method of NewLocation
     *
     * @function
     * @public
     * @returns {ReactElement} the screen with the saving animation
     */
    private returnIsSavingView(): ReactElement {
        this.saveLocation();
        return (
            <View style={styles.flexy}>
                <TopBar text="Deine Location wird gespeichert"></TopBar>
                <View style={LoadingComponentStyle.container}>
                    <LoadingComponent list={this.list} loadingType={LoadingType.location}></LoadingComponent>
                </View>
            </View>
        );
    }

    /**
     * returnSavedView Method of NewLocation
     *
     * @function
     * @public
     * @returns {ReactElement} the screen with the saved label
     */
    private returnSavedView(): ReactElement {
        return (
            <View style={LoadingComponentStyle.containerItemsCentered}>
                <Text style={LoadingComponentStyle.successLabel}>Wir haben deinen Beitrag gespeichert!</Text>
                <MoveForwardButton
                    Destination={(): string => {
                        return Routes.home;
                    }}
                    CheckCondition={(): CheckConditionType => {
                        return { isSatisfied: true, infoText: '' };
                    }}
                ></MoveForwardButton>
            </View>
        );
    }

    /**
     * returnAddingScreen Method of NewLocation
     *
     * @function
     */
    private goToAddingView(): void {
        this.setState({ screenState: this.screenStates.addingScreen });
    }

    /**
     * goToIsSavingView Method of NewLocation
     *
     * @function
     */
    private goToIsSavingView(): void {
        this.setState({ screenState: this.screenStates.isSavingScreen });
    }
}
