/**
 * An Interface for handling the database connection
 *
 * @interface
 * @author valentin_lechner
 * @typedef IBackendConnection
 * @property {() => void} initConnection initializes database connection
 */
export interface IBackendConnection {
    /**
     * Function responsible for initializing the db connection
     *
     * @function
     * @public
     * @author valentin_lechner
     */
    initConnection(): void;
}
