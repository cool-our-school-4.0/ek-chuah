import { Moods } from '../enums/enums';
/**
 * the database object ilocation
 *
 * @interface
 * @author valentin_lechner and jan_koerner
 * @typedef ILocation
 * @property {number} latitude lat
 * @property {number} longitude lon
 * @property {string} locationname name of the location
 * @property {number} locationid id of the location in the db
 * @property {string[]} tags the tags of the location
 * @property {number} relevance the likes of the location
 * @property {string} image the image as binary string
 * @property {string} description the description of the location
 */
export interface ILocation {
    latitude: number;
    longitude: number;
    locationname: string;
    locationid: number;
    tags: Moods[];
    relevance: number;
    image: string;
    description: string;
}
