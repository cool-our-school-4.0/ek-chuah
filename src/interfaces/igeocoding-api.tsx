import { IGeocodingApiMapboxObjectFeature } from './imap-objects';
import { ParamType } from '../types/map/geocoding-api/url-builder/param-type';

/**
 *
 *   Object "IGeocodingApi"
 *   The UML in <rootDir>/docs/ gives a pretty good idea of how the structure is
 *   supposed to be
 */
/**
 * @external Promise
 * @external IGeocodingApiMapboxFeature
 */
/**
 * Interface for param types for accessing the geocoding api without sdk
 *
 * @interface
 * @typedef IGeocodingApiMapboxQueryType
 * @property {string} param the parameter name
 * @property {string} varvalue the value of the parameter
 * @property {boolean} required if the parameter is required
 */
export interface IGeocodingApiMapboxQueryType {
    param: string;
    varvalue: string;
    readonly required: boolean;
}

/**
 * Mapbox interface for handling api requests
 *
 * @interface
 * @author valentin_lechner
 * @typedef IGeocodingApiMapbox
 * @property {boolean} useProximity whether to use the proximity algorithm in
 * the api request, if true loads user location
 * @property {IGeocodingApiMapboxFeature[]} currentFeatures here the loaded
 * features get saved
 */
export interface IGeocodingApiMapbox {
    useProximity: boolean;
    currentFeatures: IGeocodingApiMapboxObjectFeature[];
    /**
     * this  should call the search function and asave the results in
     * currentfeatures
     */
    queryByName(name: string, types?: ParamType[]): Promise<void>;
}
/**
 * Interface for building the api url for geocoding requests
 *
 * @interface
 * @typedef IGeocodingApiApiUrlBuilder
 * the api url and returns it
 */
export interface IGeocodingApiApiUrlBuilder {
    /**
     * builds the api string and returns it
     */
    getApiUrl(searchtext: string, proximity: boolean, types?: ParamType[]): Promise<string>;
}

/**
 * Data Handler for handling Mapbox API Requests
 *
 * @interface
 * @author valentin_lechner
 * @typedef IGeocodingApiApiDataHandler
 */
export interface IGeocodingApiApiDataHandler {
    /**
     * the api call should be implemented here
     */
    getFromUrl(url: string): Promise<IGeocodingApiMapboxObjectFeature[]>;
}
