import { IBackendConnection } from '../interfaces/ibackend-connection';
import { IBackendQuery } from '../interfaces/ibackend-query';
/**
 * An Interface for wrapping backend data and connection
 *
 * @interface
 * @author valentin_lechner
 * @typedef IBackendHandler
 * @property {IBackendConnection} backendConnection the connection handler
 * @property {IBackendQuery} backendQuery the query handler
 */
export interface IBackendHandler {
    /**
     * Handles backend connection
     */
    backendConnection: IBackendConnection;
    /**
     * Handles backend Querys
     */
    backendQuery: IBackendQuery;
}
