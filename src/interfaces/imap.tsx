/**
 * @external Promise
 */
/**
 * An Interface for Handling Location Data
 *
 * @author valentin_lechner
 * @interface
 * @typedef IMapLocationDataHandler
 */
export interface IMapLocationDataHandler {
    /**
     * if we are allowed to use the users location
     */
    getUseUserLocation(): Promise<boolean>;
    /**
     * returns the user location
     */
    getUserLocation(): Promise<IMapCoords>;
    /**
     * returns teh user location as comma seperated string (lat,lon)
     */
    getUserLocationString(): Promise<string>;
}

/**
 * An Interface for carrying Coordinates
 *
 * @interface
 * @author valentin_lechner
 * @typedef IMapCoords
 * @property {number} lat latitude
 * @property {number} lon longitude
 */
export interface IMapCoords {
    lat: number;
    lon: number;
}

/**
 * An Interface for carrying Location data, extends Coordinates by zoomlevel
 *
 * @author valentin_lechner
 * @interface
 * @typedef IMapLocation
 * @property {IMapCoords} coords the coordinates
 * @property {number} zoom the zoom level
 */
export interface IMapLocation {
    coords: IMapCoords;
    zoom: number;
}
