/**
 *   IGeocodingApi.Mapbox.Object
 *   The UML in <rootDir>/docs/ gives a pretty good idea of how the structure is supposed to be
 */

/**
 * Structure generated using https://quicktype.io/typescript/
 *
 * @author valentin_lechner
 */

/**
 * generated interface for the Mapbox API. This is the 'parent' interface for all other interfaces
 *
 * @interface
 * @author valentin_lechner
 * @typedef IGeocodingApiMapboxObjectBase
 * @property {string} type the type of data contained
 * @property {string[]} query ???
 * @property {IGeocodingApiMapboxObjectFeature[]} features the features returned from the query
 * @property {string} attribution unneeded
 */
export interface IGeocodingApiMapboxObjectBase {
    type: string;
    query: string[];
    features: IGeocodingApiMapboxObjectFeature[];
    attribution: string;
}

/**
 * 'IGeocodingApiMapboxObjectFeatures' are the places, this is the interface for controlling them
 *
 * @interface
 * @typedef IGeocodingApiMapboxObjectFeature
 * @property {string} id Identifier of the feature
 * @property {string} type the type of the feature
 * @property {string[]} place_type the place type of the feature
 * @property {number} relevance how relevant the feature is. used to sort in a search by relevance
 * @property {IGeocodingApiMapboxObjectProperties} properties the properties of the feature
 * @property {string} text text describing the feature
 * @property {string} place_name the name of the feature
 * @property {number[]} bbox? ??
 * @property {number[]} center the center coordinates of the feature
 * @property {IGeocodingApiMapboxObjectGeometry} geometry the coordinates of the feature
 * @property {IGeocodingApiMapboxObjectContext} context ???
 * @author valentin_lechner
 */
export interface IGeocodingApiMapboxObjectFeature {
    id: string;
    type: string;
    place_type: string[];
    relevance: number;
    properties: IGeocodingApiMapboxObjectProperties;
    text: string;
    place_name: string;
    bbox?: number[];
    center: number[];
    geometry: IGeocodingApiMapboxObjectGeometry;
    context: IGeocodingApiMapboxObjectContext[];
}

/**
 * Additional Place Info
 *
 * @interface
 * @author valentin_lechner
 * @typedef IGeocodingApiMapboxObjectContext
 * @property {string} id ??
 * @property {string} short_code ??
 * @property {string} wikidata ??
 * @property {string} text ??
 */
export interface IGeocodingApiMapboxObjectContext {
    id: string;
    short_code?: string;
    wikidata?: string;
    text: string;
}

/**
 * Additional Place Info
 *
 * @interface
 * @author valentin_lechner
 * @typedef IGeocodingApiMapboxObjectGeometry
 * @property {string} type ??
 * @property {number[]} the coordinates
 */
export interface IGeocodingApiMapboxObjectGeometry {
    type: string;
    coordinates: number[];
}

/**
 * Additional Place Info
 *
 * @interface
 * @author valentin_lechner
 * @typedef IGeocodingApiMapboxObjectProperties
 * @property {string} short_code ??
 * @property {boolean} landmark ??
 * @property {string} address ??
 * @property {string} category ??
 * @property {string} maki ??
 */
export interface IGeocodingApiMapboxObjectProperties {
    wikidata?: string;
    landmark?: boolean;
    address?: string;
    category?: string;
    maki?: string;
}
