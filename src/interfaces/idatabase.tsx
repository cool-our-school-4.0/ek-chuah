import { ILocation } from '../interfaces/ilocations';

/**
 * Interface Database
 *
 * @interface
 * @author valentin_lechner and jan_koerner
 * @typedef IDatabase
 * @property {() => ILocation[]} getAllLocations Load all Locations
 * @property {(id: number) => ILocation | void} getLocationById returns location matching the id, if any
 */
export interface IDatabase {
    addNewLocation(loc: ILocation): Promise<void>;
    getAllLocations(): Promise<ILocation[]>;
    getLocationById(id: number): Promise<ILocation | void>;
    likeLocationById(id: number): Promise<void>;
}
