import { ILocation } from '../interfaces/ilocations';
/**
 * @external Promise
 */
/**
 * An Interface for handling database querys
 *
 * @author valentin_lechner
 * @interface
 * @typedef IBackendQuery
 * @property {() => ILocation[]} getAllLocations Calls the query for getting all locations
 * @property {() => ILocation  | void }  getLocationById Calls the query for getting a location by id
 * @property {(loc: ILocation) => void} addNewLocation Adds A new location
 * @property {() => void} likeLocationById increase relevance of location
 */
export interface IBackendQuery {
    /**
     * Calls the query for creating a new location
     *
     * @public
     * @param {ILocation} id the location id
     * @function
     * @returns {Promise<void>} nothing
     * @author valentin_lechner
     */
    addNewLocation(loc: ILocation): Promise<void>;
    /**
     * Calls the query for getting all locations
     *
     * @public
     * @param {number} id the location id
     * @function
     * @returns {Promise<ILocation[]>} all Locations in Database
     * @author valentin_lechner
     */
    getAllLocationsAsILocation(): Promise<ILocation[]>;
    /**
     * Calls the query for getting a location by id
     *
     * @public
     * @param {number} id the location id
     * @function
     * @returns {Promise<ILocation | void>} the first location matching the id, if any
     * @author valentin_lechner
     */
    getLocationByIdAsILocation(id: number): Promise<ILocation | void>;
    /**
     * Increases the relevance of a location
     *
     * @public
     * @function
     * @param {number} id the location id
     * @returns {Promise<void>} nothing
     * @author valentin_lechner
     */
    likeLocationById(id: number): Promise<void>;
}
