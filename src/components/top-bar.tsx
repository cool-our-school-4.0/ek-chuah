import React, { ReactElement } from 'react';
import { View, Text } from 'react-native';
import { TopBarStyle } from '../styles/sheet';

/**
 * @external TopBarProps
 */

/**
 * @interface
 * @property {string} text text which will be displayed
 * @author jan_koerner
 */
interface TopBarProps {
    text: string;
}

/**
 * @function
 * @param {TopBarProps} props props
 * @returns {ReactElement} infobar at top of the page
 * @author jan_koerner
 */
export default function TopBar(props: TopBarProps): ReactElement {
    return (
        <View style={TopBarStyle.container}>
            <Text style={TopBarStyle.text}>{props.text}</Text>
        </View>
    );
}
