import MapboxGL from '@react-native-mapbox-gl/maps';
import { Feature, Geometry as g2 } from '@turf/helpers/lib/geojson';
import { GeoJsonProperties, Geometry } from 'geojson';
import React, { ReactElement } from 'react';
import { View } from 'react-native';
import LoadingComponent from '../components/loading-component';
import { LoadingType } from '../enums/enums';
import { requestLocationPermission } from '../handler/permissions-handler';
import { LoadingComponentStyle, styles } from '../styles/sheet';
import { MapOptions, MapState } from '../types/map-types';
import { fallbackLocation } from '../types/map/fallback-location';
import { IS_ANDROID } from '../utils/utils';
import { defMapApiKeyHandler } from '../handler/map-api-key-handler';
import { defMapLocationDataHandler } from '../handler/map-location-data-handler';
import SystemSetting from 'react-native-system-setting';
import { IMapCoords } from '../interfaces/imap';
import { IGeocodingApiMapboxObjectFeature } from '../interfaces/imap-objects';

/**
 * This component is a wrapper for using the mapbox api to display a map
 *
 * @class
 * @author valentin_lechner
 * @default
 * @augments React.Component
 */
export default class ShowMap extends React.Component {
    /** Saving MapState    */
    state: MapState;
    /** Save MapOptions, such as styleUrl */
    private _mapOptions: MapOptions[];
    /**
     * Create the ReactElement
     *
     * @class
     * @author valentin_lechner
     * @param {{}} props Constructorparam
     */
    constructor(props: {}) {
        super(props);

        MapboxGL.setAccessToken(defMapApiKeyHandler.getMapApiKey());

        // fills the map options with the mapbox styles available
        this._mapOptions = Object.keys(MapboxGL.StyleURL).map((key) => {
            return {
                label: key,
                // eslint-disable-next-line @typescript-eslint/no-explicit-any
                data: (MapboxGL.StyleURL as any)[key] as string
            };
        });

        // fill status with current style (default style at 0) and the need to ask for permissions
        this.state = {
            isAndroidPermissionGranted: false,
            isFetchingAndroidPermission: IS_ANDROID,
            styleUrl: this._mapOptions[0].data,
            userLocationActive: true,
            customLocation: fallbackLocation
        };
    }

    /**
     *  Converts a custom selected feature to coordinates
     *  The map needs a feature for display
     *
     * @private
     * @function
     * @returns {IGeocodingApiMapboxObjectFeature} a feature to be shown on the map
     */
    private coordinatesToFeature(coords: IMapCoords): IGeocodingApiMapboxObjectFeature {
        const feature: IGeocodingApiMapboxObjectFeature = {
            id: `${Date.now()}`,
            type: '',
            relevance: 0,
            // eslint-disable-next-line            @typescript-eslint/camelcase
            place_name: '',
            text: '',
            center: [],
            context: [],
            // eslint-disable-next-line            @typescript-eslint/camelcase
            place_type: [],
            properties: {},
            geometry: {
                type: '',
                coordinates: [coords.lon, coords.lat]
            }
        };
        return feature;
    }

    /**
     * When component did mount, the component decides if it needs to ask for permissions
     *
     * @author valentin_lechner
     * @function
     * @public
     * @returns {void} Emptyness
     */
    public async componentDidMount(): Promise<void> {
        if (IS_ANDROID) {
            const isGranted = (await requestLocationPermission()) && (await SystemSetting.isLocationEnabled());
            this.setState({
                isAndroidPermissionGranted: isGranted,
                isFetchingAndroidPermission: false,
                userLocationActive: isGranted,
                customLocation: this.state.userLocationActive
                    ? this.coordinatesToFeature(await defMapLocationDataHandler.getUserLocation())
                    : fallbackLocation
            });
        }
        MapboxGL.setTelemetryEnabled(false);
        /*
          locationManagers Job is to get the Location of the user.
          Since it has not been typed for ts, this here led to an error (TS 2339),
          which could be resolved using @ts-ignore. That also led to the test
          cases working.
          But, using @ts-ignore, it seemed that the line did not get passed into
          the app.
          Therefore, the map location service stopped working.
          While grepping for the type definitions i found an alternative in the
          userlocation docs:
          When using MapboxGL.UserLocation, set one of the following tags to
          ensure locationManager to start:
          - onUpdate
          - visible

          MapboxGL.locationManager.start();

        */
    }

    /**
     * Renders Custom Location View in the case that the user granted manually
     * added a marker or that he didn't give access to location services
     * In the case that the user didnt allow location access, the view shows a
     * defined default value
     *
     * @returns {ReactElement} the customlocationview, showing the selected
     * location
     * @function
     * @private
     * @author valentin_lechner
     */
    private renderCustomLocationView(): ReactElement {
        return (
            <View style={styles.flexy}>
                <MapboxGL.MapView
                    onPress={this._onMapPress}
                    styleURL={this.state.styleUrl}
                    style={styles.flexy}
                    localizeLabels={true}
                    surfaceView={IS_ANDROID}
                >
                    <MapboxGL.ShapeSource id={'markers'} shape={this.state.customLocation}>
                        <MapboxGL.SymbolLayer
                            id="SymbolLayer"
                            style={{
                                iconAllowOverlap: true,
                                iconImage: require('../common/MapMarker.png'),
                                iconSize: 0.1,
                                iconAnchor: 'bottom'
                            }}
                        />
                    </MapboxGL.ShapeSource>
                    <MapboxGL.Camera
                        zoomLevel={17}
                        followZoomLevel={17}
                        centerCoordinate={[
                            // center Coordinate is in order: lng, lat
                            this.state.customLocation.geometry.coordinates[0],
                            this.state.customLocation.geometry.coordinates[1]
                        ]}
                    />
                </MapboxGL.MapView>
            </View>
        );
    }

    /**
     * Renders User Location View in the case that the user
     * granted Location access
     *
     * @returns {ReactElement} the users location in a view
     * @function
     * @private
     * @author valentin_lechner
     */
    private renderUserLocationView(): ReactElement {
        return (
            <View style={styles.flexy}>
                <MapboxGL.MapView
                    onPress={this._onMapPress}
                    styleURL={this.state.styleUrl}
                    style={styles.flexy}
                    localizeLabels={true}
                    surfaceView={IS_ANDROID}
                >
                    <MapboxGL.Camera followZoomLevel={17} followUserLocation={true} followUserMode="normal" />
                    <MapboxGL.UserLocation visible={true} />
                </MapboxGL.MapView>
            </View>
        );
    }

    /**
     * Render Loading Component just to be sure that there is no weird map status where no permission has been granted
     * doesn't show a list full of funny jokes because its most probably a short-living Display
     *
     * @function
     * @returns {ReactElement} a loading screen
     * @private
     * @author valentin_lechner
     */
    private renderAwaitingPermissions(): ReactElement {
        return (
            <View style={LoadingComponentStyle.containerItemsCentered}>
                <LoadingComponent list={['Lade Daten']} loadingType={LoadingType.dateLocation}></LoadingComponent>
            </View>
        );
    }

    /**
     * Returns the view for this screen
     *
     * @author valentin_lechner
     * @public
     * @function
     * @returns {ReactElement} The View displaying a Map following the user and displaying his current location
     */
    public render(): ReactElement {
        /* Currently asking for Permissions to access location */
        if (IS_ANDROID && !this.state.isAndroidPermissionGranted) {
            if (this.state.isFetchingAndroidPermission) {
                return this.renderAwaitingPermissions();
            } else {
                if (defMapLocationDataHandler.getUseUserLocation()) {
                    return this.renderCustomLocationView();
                }
            }
        }

        return this.state.userLocationActive ? this.renderUserLocationView() : this.renderCustomLocationView();
    }

    /**
     * onMapPress Event which creates a new Feature at press location.
     *
     * @author valentin_lechner
     * @param {Feature<Geometry, GeoJsonProperties>} e the item passed
     * from the event
     */
    _onMapPress = (e: Feature<Geometry, GeoJsonProperties>): void => {
        // Mapbox appears to use different 'Geometry' Types in their API.
        // Their use is not consistent and they seem to assign a non-typed
        // property to the geojson.Geometry Object, which is Present in
        // @turf/helpers/lib/geojson.Geometry
        // Definitely NOT happy with all the casting going on here...
        const geom: g2 = (e.geometry as unknown) as g2;
        const feature: Feature = {
            id: 'UserLocation',
            type: 'Feature',
            // eslint-disable-next-line            @typescript-eslint/camelcase
            properties: {},
            // eslint-disable-next-line            @typescript-eslint/camelcase
            geometry: {
                type: geom.type,
                coordinates: [geom.coordinates[0] as number, geom.coordinates[1] as number]
            }
        };
        feature.id = `${Date.now()}`;
        this.setState({ customLocation: feature, userLocationActive: false });
    };
}
