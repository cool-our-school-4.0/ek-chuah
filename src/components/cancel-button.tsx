import React, { ReactElement } from 'react';
import { View } from 'react-native';
import { FAB } from 'react-native-paper';
import { MoveButtonStyle } from '../styles/sheet';

/**
 * Cancel Button Komponente
 *
 * @author JanaRebmann
 * @returns {ReactElement} Returns a FAB to cancel
 * @param {()=> void} props properties for the cancel button
 */
export default function CancelButton(props: { onPressFunction: () => void }): ReactElement {
    return (
        <View style={[MoveButtonStyle.viewDefault, MoveButtonStyle.viewLeft]}>
            <FAB icon={'close'} style={MoveButtonStyle.default} onPress={props.onPressFunction}></FAB>
        </View>
    );
}
