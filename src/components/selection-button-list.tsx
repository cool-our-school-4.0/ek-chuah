import React, { ReactElement } from 'react';
import { View } from 'react-native';
import SelectionButton, { SelectionRouteButton } from './selection-button';
import { CheckConditionType } from '../types/nav-button-types';
import { styles, ListStyle } from '../styles/sheet';
import AsyncStorage from '@react-native-community/async-storage';

/**
 * @external Map
 * @external Buttontype
 */

/**
 * @author jan_koerner
 * @interface
 * @typedef SelectionButtonListProps
 * @property {DefaultSelectionButtonListProps} default The default Props needed by both selectionbuttonlists
 * @property {string} storageKey The key for Asyncstorage
 */
interface SelectionButtonListProps<ButtonType> {
    default: DefaultSelectionButtonListProps<ButtonType>;
    storageKey: string;
}
/**
 * Interface for 'SelectionButtonList' Parameters
 *
 * @author jan_koerner
 * @interface
 * @typedef DefaultSelectionButtonListProps
 * @property {Buttontype} buttons array of buttons in this list, Buttontyp is generic
 * @property {boolean} selectMultiple boolean if multiple buttons can be selected / checked
 * @property {number} minAmountSelected min amount of buttons which must be selected
 * @property {boolean} columns boolean if there should be 2 colums or not
 */
interface DefaultSelectionButtonListProps<ButtonType> {
    buttons: ButtonType[];
    selectMultiple: boolean;
    minAmountSelected: number;
    twoColumns?: boolean;
}

/**
 * Interface for 'Defaultbutton'
 *
 * @author jan_koerner
 * @interface
 * @typedef DefaultButton
 * @property  {string} buttonText the related text to be displayed
 * @property {string} id the related id
 */
interface DefaultButton {
    buttonText: string;
    id: string;
}
/**
 * Interface for 'DefaultRouteButton'
 *
 * @author jan_koerner
 * @interface
 * @typedef DefaultRouteButton
 * @property  {DefaultButton} button the related default button
 * @property {string} routeName name of the Route
 */
interface DefaultRouteButton {
    button: DefaultButton;
    routeName: string;
}

/**
 * @param {number} minAmount the min amount which must be selected
 * @param {SelectionButton[]} btns all buttons of this list
 * @returns {CheckConditionType} boolean if min amount of buttons is selected
 */
function minAmountSelected(minAmount: number, btns: Map<string, boolean>): CheckConditionType {
    let currentAmountSelected = 0;
    let currentStatus: CheckConditionType;
    btns.forEach((value) => {
        if (value) {
            currentAmountSelected += 1;
        }
    });
    if (currentAmountSelected >= minAmount) {
        currentStatus = { isSatisfied: true, infoText: '' };
        return currentStatus;
    }
    currentStatus = { isSatisfied: false, infoText: 'Es muss genau eine Option ausgewählt werden' };
    return currentStatus;
}

/**
 * @param {string} pressedBtn id of the pressed buttton
 * @param {Map<string,boolean>} allBtns map of all buttons
 * @param {boolean} selectMultiple bool if several buttons can be checked at the same time
 * @returns {void} Emptyness
 */
function pressed(pressedBtn: string, allBtns: Map<string, boolean>, selectMultiple: boolean): Map<string, boolean> {
    const checked = allBtns.get(pressedBtn);
    if (!selectMultiple) {
        allBtns.forEach((_, key) => {
            allBtns.set(key, false);
        });
    }
    allBtns.set(pressedBtn, !checked);
    return allBtns;
}

/**
 *
 * @class
 * @author jan_koerner
 * @param {SelectionButtonListProps<DefaultButton>} props properies
 */
export class SelectionButtonList extends React.Component<SelectionButtonListProps<DefaultButton>> {
    state: { checkedButtons: Map<string, boolean> } = { checkedButtons: new Map() };
    constructor(props: SelectionButtonListProps<DefaultButton>) {
        super(props);
        this.pressed = this.pressed.bind(this);
        this.minAmountSelected = this.minAmountSelected.bind(this);
        this.props.default.buttons.forEach((btn) => {
            this.state.checkedButtons.set(btn.id, false);
        });
    }

    /**
     *
     * @function
     * @author jan_koerner
     * @returns {ReactElement} the rendered buttons
     */
    render(): ReactElement {
        return (
            <View style={[this.props.default.twoColumns ? ListStyle.twoColumns : styles.flexy]}>
                {this.props.default.buttons.map((btn) => {
                    return (
                        <SelectionButton
                            key={btn.id}
                            buttonText={btn.buttonText}
                            id={btn.id}
                            pressed={(): void => this.pressed(btn.id)}
                            isChecked={this.state.checkedButtons.get(btn.id) ?? false}
                            height={this.props.default.twoColumns ? this.getButtonHeight() : undefined}
                        />
                    );
                })}
            </View>
        );
    }
    /**
     * Function to calculate the Button height based on the amount of buttons
     *
     * @function
     * @author jan_koerner
     * @returns {string} height of the buttons
     */
    getButtonHeight(): string {
        const height = 100 / Math.ceil(this.props.default.buttons.length / 2);
        return height.toString() + '%';
    }

    /**
     * Function which gets called if one Button gets pressed
     *
     * @function
     * @author jan_koerner
     * @param {string} id the button id
     * @returns {void} Emptyness
     */
    pressed(id: string): void {
        const buttons = pressed(id, this.state.checkedButtons, this.props.default.selectMultiple);
        this.setState({ checkedButtons: buttons });
    }
    /**
     *
     * @function
     * @author jan_koerner
     * @returns {CheckConditionType} state if condition is satisfied or not
     */
    minAmountSelected(): CheckConditionType {
        return minAmountSelected(this.props.default.minAmountSelected, this.state.checkedButtons);
    }
    /**
     * Saves the current selected Button/s
     *
     * @function
     * @author jan_koerner
     * @returns {void} Emptyness
     */
    async writeStorage(): Promise<void> {
        const checkedIds: string[] = [];
        this.state.checkedButtons.forEach((value: boolean, key: string) => {
            if (value) {
                checkedIds.push(key);
            }
        });
        return await AsyncStorage.setItem(this.props.storageKey, JSON.stringify(checkedIds));
    }
}

/**
 * Button List Class if the buttons represent different routes
 *
 * @class
 * @author jan_koerner
 * @param {DefaultSelectionButtonListProps} props properties
 */
export class SelectionRouteButtonList extends React.Component<DefaultSelectionButtonListProps<DefaultRouteButton>> {
    state: { checkedButtons: Map<string, boolean> } = { checkedButtons: new Map() };
    constructor(props: DefaultSelectionButtonListProps<DefaultRouteButton>) {
        super(props);
        this.pressed = this.pressed.bind(this);
        this.minAmountSelected = this.minAmountSelected.bind(this);
        this.props.buttons.forEach((btn) => {
            this.state.checkedButtons.set(btn.button.id, false);
        });
    }

    /**
     *
     * @function
     * @author jan_koerner
     * @returns {ReactElement} the renderd buttons
     */
    render(): ReactElement {
        return (
            <View style={styles.flexy}>
                {this.props.buttons.map((btn) => {
                    return (
                        <SelectionRouteButton
                            key={btn.button.id}
                            route={btn.routeName}
                            button={{
                                buttonText: btn.button.buttonText,
                                id: btn.button.id,
                                pressed: (): void => this.pressed(btn.button.id),
                                isChecked: this.state.checkedButtons.get(btn.button.id) ?? false
                            }}
                        />
                    );
                })}
            </View>
        );
    }
    /**
     * Function which gets called if one Button gets pressed
     *
     * @function
     * @author jan_koerner
     * @param {string} id the id of the pressed button
     * @returns {void} Emptyness
     */
    pressed(id: string): void {
        const buttons = pressed(id, this.state.checkedButtons, this.props.selectMultiple);
        this.setState({ checkedButtons: buttons });
    }
    /**
     * Function which returns the current active Route
     *
     * @function
     * @author jan_koerner
     * @returns {string | undefined} the current route if one is selected
     */
    getCurrentRoute(): string | undefined {
        const checkedButton = this.props.buttons.find((btn): DefaultRouteButton | undefined => {
            if (this.state.checkedButtons.get(btn.button.id)) {
                return btn;
            }
            return undefined;
        });
        return checkedButton?.routeName ?? undefined;
    }
    /**
     *
     * @function
     * @author jan_koerner
     * @returns {CheckConditionType} the current state if the condition is satisfied or not
     */
    minAmountSelected(): CheckConditionType {
        return minAmountSelected(this.props.minAmountSelected, this.state.checkedButtons);
    }
}
