import React, { ReactElement } from 'react';
import { View, Text } from 'react-native';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { ButtonStyle, styles } from '../styles/sheet';

/**
 *
 * @external SelectionRouteButtonProps
 * @external SelectionButtonProps
 */
/**
 * Interface for 'SelectionRouteButton' Parameters
 *
 * @author jan_koerner
 * @interface
 * @typedef Props
 * @property {string} route the route name of the next screen
 * @property {SelectionButtonProps} button  the related selectionbuttonprops
 */
interface SelectionRouteButtonProps {
    route: string;
    button: SelectionButtonProps;
}

/**
 * Interface for 'SelectionRouteButton' Parameters
 *
 * @author jan_koerner
 * @interface
 * @typedef Props
 * @property  {string} buttonText the related text to be displayed
 * @property {string} id the related id
 * @property {(button:string)=>void} pressed the function which will be executed if the button is pressed
 * @property {string} height the height of the buttons, if there are two columns
 */
interface SelectionButtonProps {
    buttonText: string;
    id: string;
    pressed: (button: string) => void;
    isChecked: boolean;
    height?: string;
}

/**
 * This class extends the SelectionButton class with an additional route prop
 *
 * @class
 * @author jan_koerner
 * @param {SelectionRouteButtonProps} props
 * @
 */
export class SelectionRouteButton extends React.Component<SelectionRouteButtonProps> {
    public render(): ReactElement {
        return (
            <SelectionButton
                buttonText={this.props.button.buttonText}
                id={this.props.button.id}
                isChecked={this.props.button.isChecked}
                pressed={this.props.button.pressed}
            ></SelectionButton>
        );
    }
}
/**
 * This is the Class of the SelectionButtons
 *
 * @class
 * @author jan_koerner
 * @param  {SelectionButtonProps} props
 * @property {boolean} isChecked boolean if button is selected
 */
export default class SelectionButton extends React.Component<SelectionButtonProps> {
    public render(): ReactElement {
        return (
            <View
                style={[
                    this.props.height ? [{ height: this.props.height }, ButtonStyle.twoColumns] : styles.flexy,
                    { justifyContent: 'space-around' }
                ]}
            >
                <TouchableWithoutFeedback
                    style={[ButtonStyle.container, ButtonStyle.default]}
                    onPress={(): void => {
                        this.props.pressed(this.props.id);
                    }}
                >
                    <Text style={[this.props.isChecked ? ButtonStyle.textChecked : ButtonStyle.textUnchecked]}>
                        {this.props.buttonText}
                    </Text>
                </TouchableWithoutFeedback>
            </View>
        );
    }
}
