import React, { ReactElement } from 'react';
import { View } from 'react-native';
import { FAB, Colors } from 'react-native-paper';
import { MoveButtonStyle } from '../styles/sheet';
import { defParseDatabase as Database } from '../backend/database';
import Snackbar from 'react-native-snackbar';

/**
 * Like Button Komponente
 *
 * @author JanaRebmann
 * @returns {ReactElement} Returns a FAB to like a location
 * @param {number} props location which will be liked
 */
export default function LikeButton(props: { locationId: number }): ReactElement {
    return (
        <View style={[MoveButtonStyle.viewDefault]}>
            <FAB
                icon={'heart'}
                style={[MoveButtonStyle.viewDefault, MoveButtonStyle.smaller]}
                onPress={async (): Promise<void> => {
                    try {
                        const location = await Database.getLocationById(props.locationId);
                        await Database.likeLocationById(props.locationId);

                        Snackbar.show({
                            text: location.locationname + ' wurde ein Like vergeben!',
                            duration: Snackbar.LENGTH_SHORT,
                            backgroundColor: Colors.blue900,
                            textColor: Colors.white
                        });
                    } catch (error) {
                        Snackbar.show({
                            text: 'Error! Liken ist nicht möglich',
                            duration: Snackbar.LENGTH_SHORT,
                            backgroundColor: Colors.blue900,
                            textColor: Colors.white
                        });
                    }
                }}
            ></FAB>
        </View>
    );
}
