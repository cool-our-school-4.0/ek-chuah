import React, { ReactElement } from 'react';
import { View } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { FAB } from 'react-native-paper';
import { MoveButtonStyle } from '../styles/sheet';

/**
 * Interface for moving back FAB
 *
 * @interface
 * @typedef MoveBackwardButtonProps
 * @property {string} Label The Text of the Button
 * @author valentin_lechner
 */
interface MoveBackwardButtonProps {
    Label?: string;
}

/**
 * Zurück Button Komponente
 *
 * @author jan_koerner
 */

/**
 * Move Backward button
 *
 * @author jan_koerner
 * @param {MoveBackwardButtonProps} props Parameter containing an optional label
 * @returns {ReactElement} Returns a FAB to go back
 */
export default function MoveBackwardButton(props: MoveBackwardButtonProps): ReactElement {
    const nav = useNavigation();
    /**
     * Shortcut for goBack
     *
     * @author jan_koerner
     * @function
     * @returns {void} Emptyness
     */
    function goBack(): void {
        nav.goBack();
    }

    return (
        <View style={[MoveButtonStyle.viewDefault, MoveButtonStyle.viewLeft]}>
            <FAB
                icon={'arrow-left-bold'}
                label={props.Label ?? ''}
                style={MoveButtonStyle.default}
                onPress={goBack}
            ></FAB>
        </View>
    );
}
