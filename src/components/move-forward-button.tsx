import React, { ReactElement } from 'react';
import { View } from 'react-native';
import { FAB, Colors } from 'react-native-paper';
import { useNavigation } from '@react-navigation/native';
import Snackbar from 'react-native-snackbar';
import { CheckConditionType } from '../types/nav-button-types';
import { MoveButtonStyle } from '../styles/sheet';

/**
 *
 * @external MoveForwardButtonProps
 */

/**
 * Interface for 'moveForward' Parameters
 *
 * @author valentin_lechner
 * @interface
 * @typedef Props
 * @property  {string} Destination the next screen to be displayed
 * @property {string} Label an optional label to be displayed next to the icon
 * @property {() => boolean} CheckCondition A function to check if the condition to change to the next screen is fulfilled
 * @property {() => void} save A function that can be implemented to save stuff on go-next
 */

// TODO Props in camelCase
interface MoveForwardButtonProps {
    Destination: () => string | undefined;
    Label?: string;
    CheckCondition: () => CheckConditionType | undefined;
    stashSave?: () => void;
    higherPos?: boolean;
}

/**
 * Vorwärts-Button Komponente, wird in den jeweiligen Screens direkt eingebunden
 *
 * @author jan_koerner
 * @returns {ReactElement} FAB to go to the next screen
 * @param {MoveForwardButtonProps} props properties for this function
 */
export default function MoveForwardButton(props: MoveForwardButtonProps): ReactElement {
    const nav = useNavigation();
    /**
     * Opens a snackbar with infotext ("Toast" in android) - A small notification on the bottom of the screen
     *
     * @param {string} snackText The text to be displayed
     * @returns {void} Emptyness
     * @author jan_koerner
     * @function
     */
    function OpenSnackbar(snackText: string): void {
        Snackbar.show({
            text: snackText,
            duration: Snackbar.LENGTH_SHORT,
            backgroundColor: Colors.blue900,
            textColor: Colors.white
        });
    }
    /**
     * Navigates to the specified Route
     *
     * @returns {void} Emptyness
     * @author jan_koerner
     * @function
     */
    function navigateTo(): void {
        const status = props.CheckCondition();
        if (props.stashSave !== undefined) {
            props.stashSave();
        }
        if (status) {
            if (status.isSatisfied) {
                const destination = props.Destination();
                if (destination !== undefined) {
                    nav.navigate(destination);
                } else {
                    throw new Error('Ein kritischer Fehler wurde festgestellt Destination');
                }
            } else {
                OpenSnackbar(status.infoText);
            }
        } else {
            throw new Error('Ein kritischer Fehler wurde festgestellt');
        }
    }

    return (
        <View
            style={[MoveButtonStyle.viewDefault, MoveButtonStyle.viewRight, props.higherPos ? { bottom: '15%' } : {}]}
        >
            <FAB
                icon={'arrow-right-bold'}
                label={props.Label ?? ''}
                style={MoveButtonStyle.default}
                onPress={navigateTo}
            ></FAB>
        </View>
    );
}
