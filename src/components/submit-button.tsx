import React, { ReactElement } from 'react';
import { View } from 'react-native';
import { FAB } from 'react-native-paper';
import { MoveButtonStyle } from '../styles/sheet';

/**
 * Submit Button Komponente
 *
 * @author JanaRebmann
 * @returns {ReactElement} Returns a FAB to submit
 * @param {()=>void} props properties for the submit button
 */
export default function SubmitButton(props: { onPressFunction: () => void }): ReactElement {
    return (
        <View style={[MoveButtonStyle.viewDefault, MoveButtonStyle.viewRight]}>
            <FAB icon={'check-bold'} style={MoveButtonStyle.default} onPress={props.onPressFunction}></FAB>
        </View>
    );
}
