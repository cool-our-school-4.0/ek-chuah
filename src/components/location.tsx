import React, { ReactElement } from 'react';
import { View, Text, Image, ImageSourcePropType } from 'react-native';
import images from '../data/locations/images';
import { styles, LocationStyles } from '../styles/sheet';
import { Chip, Headline, Colors } from 'react-native-paper';
import { ILocation } from '../interfaces/ilocations';
import { moodicons } from '../enums/enums';

/**
 * The Location Properties
 *
 * @interface
 * @typedef LocationProps
 * @property {string} Label The Text of the Button
 * @author valentin_lechner
 */
export interface LocationProps {
    locationdbObject: ILocation;
}

/**
 * Funktion der Location Komponente
 *
 * @class
 * @author JanaRebmann
 * @param {LocationProps} props Parameter containing the properties of the location
 */
export default function Location(props: LocationProps): ReactElement {
    return (
        <View style={[styles.flexy, { backgroundColor: Colors.indigo50 }]}>
            <View style={styles.flexy}>
                {props.locationdbObject.image ? (
                    <Image
                        source={images[props.locationdbObject.image] as ImageSourcePropType}
                        style={LocationStyles.image}
                    ></Image>
                ) : (
                    <Image
                        source={require('../data/locations/images/404_not_found_4x.png')}
                        style={LocationStyles.image}
                    ></Image>
                )}
            </View>
            <View style={LocationStyles.contentContainer}>
                <Headline style={LocationStyles.blueHeadline}>{props.locationdbObject.locationname}</Headline>
                <Text style={LocationStyles.description}>{props.locationdbObject.description}</Text>
                <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                    {props.locationdbObject.tags.map((tag) => {
                        return (
                            <Chip key={tag} icon={moodicons.get(tag)}>
                                {tag}
                            </Chip>
                        );
                    })}
                </View>
            </View>
        </View>
    );
}

/*<Subheading>{props.StayInHours ? 'Empfohlene Aufenthaltsdauer:' + props.StayInHours : ''}</Subheading>
<Subheading style={styles.text}>�ffnungszeiten: {props.OpeningHours}</Subheading>*/
