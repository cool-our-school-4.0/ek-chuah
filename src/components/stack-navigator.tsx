import React, { ReactElement } from 'react';
import { createStackNavigator, CardStyleInterpolators } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import Home from '../screens/home';
import TimePeriod from '../screens/time-period';
import ChooseLocation from '../screens/choose-location';
import DateLocations from '../screens/date-locations';
import Mood from '../screens/mood';
import NewLocation from '../screens/new-location';
import SurpriseMeOrNot from '../screens/surprise-me-or-not';
import ComputeRoute from '../screens/compute-route';
import TourRouteTabNavigator from '../screens/tour-route-tab-navigator';
import { Routes } from '../enums/enums';

/**
 * Funktion die einen StackNavigator implementiert
 *
 * @author jan_koerner
 * @returns {ReactElement} Stacknavigator for the App
 * @exports
 * @default
 * @function
 */
export default function StackNavigator(): ReactElement {
    const Stack = createStackNavigator();
    return (
        <NavigationContainer>
            <Stack.Navigator
                initialRouteName="Home"
                headerMode="none"
                screenOptions={{
                    cardStyleInterpolator: CardStyleInterpolators.forNoAnimation
                }}
            >
                <Stack.Screen name={Routes.home} component={Home} />
                <Stack.Screen name={Routes.timePeriod} component={TimePeriod} />
                <Stack.Screen name={Routes.chooseLocation} component={ChooseLocation} />
                <Stack.Screen name={Routes.datelocations} component={DateLocations} />
                <Stack.Screen name={Routes.mood} component={Mood} />
                <Stack.Screen name={Routes.newLocation} component={NewLocation} />
                <Stack.Screen name={Routes.surpriseMeOrNot} component={SurpriseMeOrNot} />
                <Stack.Screen name={Routes.computeRoute} component={ComputeRoute} />
                <Stack.Screen name={Routes.routeTour} component={TourRouteTabNavigator} />
            </Stack.Navigator>
        </NavigationContainer>
    );
}
