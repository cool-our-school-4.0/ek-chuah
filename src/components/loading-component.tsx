import React, { ReactElement } from 'react';
import { View, Text } from 'react-native';
import { ActivityIndicator, Colors, ProgressBar, IconButton } from 'react-native-paper';
import { LoadingComponentStyle, styles } from '../styles/sheet';
import { LoadingType } from '../enums/enums';

/**
 * Klasse der LoadingScreen-View
 *
 * @class
 * @author JanaRebmann
 * @augments LoadingProps
 * @augments {number}
 */
export default class LoadingComponent extends React.Component<
    { list: string[]; loadingType: string },
    { counter: number }
> {
    state = { counter: 0 };

    private intervalID: NodeJS.Timeout = setInterval(() => {
        if (this.props.loadingType === LoadingType.route) {
            if (this.state.counter === this.props.list.length - 1) {
                this.setState({ counter: 0 });
            } else {
                this.setState({ counter: this.state.counter + 1 });
            }
        } else {
            this.setState({ counter: this.state.counter + 0.1 });
        }
    }, 1000);

    /**
     * Render Method of Loadingcomponent
     *
     * @function
     * @public
     * @returns {ReactElement} The view of a loadingscreen
     */
    public render(): ReactElement {
        let loadingElement;
        if (this.props.loadingType === LoadingType.route) {
            loadingElement = (
                <View style={LoadingComponentStyle.loadingElementOuterView}>
                    <ActivityIndicator animating={true} color={Colors.cyan500} size={'large'} />
                    <Text style={styles.centeredText}> {this.props.list[this.state.counter]} </Text>
                </View>
            );
        } else if (this.props.loadingType === LoadingType.location) {
            loadingElement = (
                <View style={LoadingComponentStyle.loadingElementOuterView}>
                    <View style={LoadingComponentStyle.loadingElementInnerView}>
                        <IconButton style={LoadingComponentStyle.icon} icon="cellphone-android" size={80}></IconButton>
                        <ProgressBar
                            color={Colors.cyan500}
                            style={LoadingComponentStyle.progress}
                            progress={this.state.counter}
                        />
                        <IconButton style={LoadingComponentStyle.icon} icon="cloud" size={80}></IconButton>
                    </View>
                    <Text style={LoadingComponentStyle.successLabel}> {this.props.list[0]} </Text>
                </View>
            );
        } else {
            loadingElement = (
                <View style={LoadingComponentStyle.loadingElementOuterView}>
                    <ActivityIndicator animating={true} color={Colors.cyan500} size={'large'} />
                    <Text style={styles.centeredText}> {this.props.list[0]} </Text>
                </View>
            );
        }
        return loadingElement;
    }

    componentWillUnmount(): void {
        clearInterval(this.intervalID);
    }
}
