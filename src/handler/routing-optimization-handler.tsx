import { ILocation } from '../interfaces/ilocations';
import { solve } from 'salesman.js';

/**
 * Class for creating a route using
 * the optimization api to build the
 * shortest path
 *
 * @author valentin_lechner
 * @class
 */
export class RoutingOptimization {
    /* Class Var Containing the baseurl, will be built into fullUrl in _fullUrl*/
    /**
     * Can be called to get the shortest route for up to twelve locations
     *
     * @function
     * @public
     * @async
     * @param {ILocation[]} locations the locations to be sorted for the
     * shortest path (max 12)
     * @returns {ILocation[]} Ordered Locations
     */
    public getShortestPath(locations: ILocation[]): ILocation[] {
        const retval: ILocation[] = new Array<ILocation>();
        const points: { id: number; x: number; y: number }[] = new Array<{ id: number; x: number; y: number }>();

        for (const location of locations) {
            points.push({ id: location.locationid, x: location.latitude, y: location.longitude });
        }
        const solution = solve(points).map((i) => points[i]);
        for (const point of solution) {
            for (const location of locations) {
                if (point.x === location.latitude) {
                    retval.push(location);
                }
            }
        }
        return retval;
    }
}
