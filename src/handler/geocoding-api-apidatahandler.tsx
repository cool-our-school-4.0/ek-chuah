import { IGeocodingApiApiDataHandler } from '../interfaces/igeocoding-api';
import { IGeocodingApiMapboxObjectBase, IGeocodingApiMapboxObjectFeature } from '../interfaces/imap-objects';

/**
 * @external Promise
 */
/**
 * Class responsible for handling data to/from a map api through use of url
 * + accessToken
 *
 * @class
 * @author valentin_lechner
 */
export class GeocodingApiApiDataHandler implements IGeocodingApiApiDataHandler {
    /**
     * Function responsible for returning the object from an url
     *
     * @function
     * @author valentin_lechner
     * @public
     * @param {string} url ApiUrl to call
     * @returns {Promise<IGeocodingApiMapboxObjectFeature[]>} A collection of
     * features which are the result of the api request
     */
    public async getFromUrl(url: string): Promise<IGeocodingApiMapboxObjectFeature[]> {
        let retval: IGeocodingApiMapboxObjectFeature[] = new Array<IGeocodingApiMapboxObjectFeature>();
        try {
            await this.get(url).then((r) => {
                retval = r.features;
            });
        } catch (e) {
            console.error(`Promise \'getFromUrl\' mit Fehler abgebrochen: ${e}`);
        }
        return retval;
    }

    /**
     * Load URL Response
     *
     * @param {string} url apiurl
     * @returns {Promise<IGeocodingApiMapboxObjectBase>} The base Mapbox Api
     * Object containing the features as a result of the search
     * @function
     * @private
     * @async
     */
    private async get(url: string): Promise<IGeocodingApiMapboxObjectBase> {
        return fetch(url, { method: 'GET' }).then((response) => {
            return response.json();
        });
    }
}
/** The default GeocodingAPiApiDataHandler */
export const defGeocodingApiApiDatahandler: IGeocodingApiApiDataHandler = new GeocodingApiApiDataHandler();
