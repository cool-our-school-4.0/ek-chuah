import Config from '../config.json';
import { ConfigEnum } from '../enums/enums';
import { EUndefinedConfigException } from '../exceptions/edata';

export class ConfigFileHandler {
    /* Cacht den Zugriff auf das Config File */
    private _cache: Map<string, string> = new Map();

    public getEntryByName(name: ConfigEnum): string {
        let tmp = this._cache.get(name);

        if (tmp === undefined) {
            tmp = Config[name];
        }

        if (tmp === undefined) {
            throw new EUndefinedConfigException(name);
        }

        this._cache.set(name, tmp);
        return tmp;
    }
}

export const defConfigHandler = new ConfigFileHandler();
