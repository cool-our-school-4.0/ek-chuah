import { IBackendHandler } from '../interfaces/ibackend-handler';
import { IBackendConnection } from '../interfaces/ibackend-connection';
import { IBackendQuery } from '../interfaces/ibackend-query';
import { defParseConnector } from '../backend/parse-connection';
import { defParseQuery } from '../backend/parse-query';
/**
 * A class implementing the backend handler for parse
 *
 * @class
 * @author valentin_lechner
 * @implements {IBackendHandler}
 */
export class ParseBackendHandler implements IBackendHandler {
    public backendConnection: IBackendConnection;
    public backendQuery: IBackendQuery;

    constructor(backendConnection: IBackendConnection, backendQuery: IBackendQuery) {
        this.backendConnection = backendConnection;
        this.backendQuery = backendQuery;
    }
}
export const defParseBackendHandler = new ParseBackendHandler(defParseConnector, defParseQuery);
