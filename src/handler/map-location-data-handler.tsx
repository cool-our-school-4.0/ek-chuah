import { IMapLocationDataHandler, IMapCoords } from '../interfaces/imap';
import { requestLocationPermission } from './permissions-handler';
import RNLocation from 'react-native-location';
import { fallbackLocation } from '../types/map/fallback-location';

/**
 * @external IMapLocationDataHandler
 * @external Promise
 */

/**
 * Class implementing the locationdatahandling
 *
 * @class
 * @author valentin_lechner
 * @implements {IMapLocationDataHandler}
 */
export class MapLocationDataHandler implements IMapLocationDataHandler {
    /**
     * Returns if User agreed to accessing loc, lazy loaded
     *
     * @function
     * @public
     * @returns {boolean} if user allowed to use location
     */
    async getUseUserLocation(): Promise<boolean> {
        return (
            requestLocationPermission()
                .then((r) => {
                    return r;
                })
                .catch((e) => {
                    console.error(e);
                    return false;
                }) &&
            (await this.getUserLocation().catch((e) => {
                console.error(e);
                return;
            })) !== null
        );
    }

    /**
     * Returns user location, if user didn't agree to getting loc, then we return a default value
     *
     * @function
     * @returns {Promise<IMapCoords>} The current coordinates of the user
     */
    async getUserLocation(): Promise<IMapCoords> {
        let tmp: IMapCoords = {
            lon: fallbackLocation.geometry.coordinates[0],
            lat: fallbackLocation.geometry.coordinates[1]
        };
        await RNLocation.getLatestLocation()
            .then((r) => {
                if (r?.latitude !== undefined && r?.longitude !== undefined) {
                    tmp = { lat: r.latitude, lon: r.longitude };
                } else {
                    tmp = {
                        lon: fallbackLocation.geometry.coordinates[0],
                        lat: fallbackLocation.geometry.coordinates[1]
                    };
                }
                return;
            })
            .catch((e) => {
                console.error(e);
                return;
            });
        return tmp;
    }

    /**
     * Same as getUserLocation, but returns a string as promise
     * format: lat,lon
     *
     * @function
     * @public
     * @author valentin_lechner
     * @returns {Promise<string>} The Location as comma seperated value
     * @async
     */
    public async getUserLocationString(): Promise<string> {
        let ret: string = '';
        await this.getUserLocation().then((r: IMapCoords) => {
            ret = `${r.lat},${r.lon}`;
        });
        return ret;
    }
}
/** The default location handler*/
export const defMapLocationDataHandler = new MapLocationDataHandler();
