import RNLocation from 'react-native-location';
/**
 * Function to request Location Permissions
 *
 * @author valentin_lechner
 * @function
 */
import { PermissionsAndroid } from 'react-native';
/**
 *
 */
export async function requestLocationPermission(): Promise<boolean> {
    try {
        const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            await RNLocation.configure({
                maxWaitTime: 5,
                distanceFilter: 100,
                desiredAccuracy: {
                    android: 'lowPower',
                    ios: 'hundredMeters'
                },
                androidProvider: 'standard',
                allowsBackgroundLocationUpdates: false
            });
            return true;
        } else {
            return false;
        }
    } catch (err) {
        console.warn(err);
        return false;
    }
}
