import { defConfigHandler } from './config-file-handler';
import { ConfigEnum } from '../enums/enums';

export class MapApiKeyHandler {
    /**
     * Function returning the API Key
     *
     * @author valentin_lechner
     * @function
     * @returns {string} The apikey
     */
    public getMapApiKey(): string {
        return defConfigHandler.getEntryByName(ConfigEnum.mapApiKey);
    }
}
export const defMapApiKeyHandler = new MapApiKeyHandler();
