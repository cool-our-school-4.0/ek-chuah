import { defConfigHandler } from './config-file-handler';
import { ConfigEnum } from '../enums/enums';

export class BackendApiKeyHandler {
    public getApiKey(): string {
        return defConfigHandler.getEntryByName(ConfigEnum.backendApiKey);
    }
}
export const defBackendApiKeyHandler = new BackendApiKeyHandler();
