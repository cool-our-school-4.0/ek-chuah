import { TourDuration, SurpriseFactor, Moods } from '../enums/enums';
import { isPointWithinRadius } from 'geolib';
import { defParseDatabase as Database } from '../backend/database';
import _ from 'lodash';
import { ILocation } from '../interfaces/ilocations';

/**
 * @external Promise
 */
/**
 * @interface
 * @author jan_koerner
 * @typedef ReturnedLocations
 * @property {number[]} locations the location id's
 * @property {number} amountUserSelect Number how many items the user can select
 * @property {number} min Min Amount of Locations
 * @property {number} max Max Amount of Locations
 */
export interface ReturnedLocations {
    locations: ILocation[];
    amountUserSelect: number;
    min: number;
    max: number;
    message?: string;
}

/**
 * Class which handles the Locations for the DateLocation Screen
 *
 * @class
 * @author jan_koerner & valentin_lechner
 * @param {TourDuration} duration the tour duration
 * @param {SurpriseFactor} surpriseFactor the surprisefactor
 * @param {Moods} moods the selected Moods
 * @param {number[]} startLocation long and lat from the tour start point
 */

export default class DateLocationHandler {
    private _duration: TourDuration;
    private _surpriseFactor: SurpriseFactor;
    private _moods: Moods[];
    private _startLocation: number[];

    constructor(duration: TourDuration, surpriseFactor: SurpriseFactor, moods: Moods[], startLocation: number[]) {
        this._duration = duration;
        this._surpriseFactor = surpriseFactor;
        this._moods = moods;
        this._startLocation = startLocation;
    }
    /**
     * Function which returns possible Locations for a Tour
     *
     * @function
     * @author valentin_lechner & jan_koerner
     * @async
     * @returns {Promise<ReturnedLocations>} Locations which are returned to the DateLocationsScreen
     */
    public async getLocations(): Promise<ReturnedLocations> {
        if (this._surpriseFactor === SurpriseFactor.full) {
            return await this.fullSurprise();
        } else if (this._surpriseFactor === SurpriseFactor.small) {
            return await this.smallSurprise();
        } else if (this._surpriseFactor === SurpriseFactor.none) {
            return await this.noSurprise();
        } else {
            console.error('Error in getLocations', this._surpriseFactor);
            return { amountUserSelect: -1, locations: [], max: -1, min: -1 };
        }
    }

    /**
     * Function which returns Locations if user has selected FullSurprise, thus the amount he can select is 0
     *
     * @function
     * @private
     * @author valentin_lechner & jan_koerner
     * @async
     * @returns {Promise<ReturnedLocations>} Locations which are returned to the DateLocationsScreen
     */
    private async fullSurprise(): Promise<ReturnedLocations> {
        const returnedLocations: ReturnedLocations = await this.getReturnedLocations();

        if (returnedLocations.locations.length > 0) {
            returnedLocations.amountUserSelect = 0;
        }

        return returnedLocations;
    }
    /**
     * Function which returns Locations if user has selected noSurprise, thus he can swipe all Locations
     *
     * @function
     * @private
     * @async
     * @author valentin_lechner & jan_koerner
     * @returns {Promise<ReturnedLocations>} Locations which are returned to the DateLocationsScreen
     */
    private async noSurprise(): Promise<ReturnedLocations> {
        const returnedLocations: ReturnedLocations = await this.getReturnedLocations();

        if (returnedLocations.locations.length > 0) {
            returnedLocations.amountUserSelect = returnedLocations.locations.length;
        }

        return returnedLocations;
    }
    /**
     * Function which returns Locations if user has selected smallSurprise, thus he can swipe half of the possible Locations
     *
     * @function
     * @private
     * @author valentin_lechner & jan_koerner
     * @returns {Promise<ReturnedLocations>} Locations which are returned to the DateLocationsScreen
     * @async
     */
    private async smallSurprise(): Promise<ReturnedLocations> {
        const returnedLocations: ReturnedLocations = await this.getReturnedLocations();

        if (returnedLocations.locations.length > 0) {
            returnedLocations.amountUserSelect = returnedLocations.locations.length / 2;
        }

        return returnedLocations;
    }

    /**
     * Function which returns the Locations, filtered by Moods + Min & Max Values
     *
     * @function
     * @private
     * @author valentin_lechner & jan_koerner
     * @returns {Promise<ReturnedLocations>} Locations which are returned to the DateLocationsScreen
     * @async
     */
    private async getReturnedLocations(): Promise<ReturnedLocations> {
        const returnedLocations: ReturnedLocations = { amountUserSelect: -1, locations: [], min: -1, max: -1 };
        const eligibleLocations: ILocation[] = await this.getEligibleLocations();

        returnedLocations.min = this.getMinAmountLocations(eligibleLocations.length);
        returnedLocations.max = this.getMaxAmountLocations();

        returnedLocations.locations = this.filterByMoods(
            eligibleLocations,
            returnedLocations.min,
            returnedLocations.max
        );
        return returnedLocations;
    }
    /**
     * Function which returns the possible Locations within a certain radius
     *
     * @function
     * @private
     * @author valentin_lechner & jan_koerner
     * @async
     * @returns {Promise<ILocation[]>} Locations which are in a certain radius
     */
    private async getEligibleLocations(): Promise<ILocation[]> {
        const radius = this.getRadius();
        const eligibleLocations: ILocation[] = new Array<ILocation>();
        return await Database.getAllLocations().then((results) => {
            results.forEach((element) => {
                if (
                    isPointWithinRadius(
                        { latitude: this._startLocation[1], longitude: this._startLocation[0] },
                        {
                            latitude: element.latitude,
                            longitude: element.longitude
                        },
                        radius
                    )
                ) {
                    eligibleLocations.push(element);
                }
            });
            return eligibleLocations;
        });
    }
    /**
     * Function which selects Locations based on the selected Moods
     *
     * @function
     * @private
     * @author valentin_lechner & jan_koerner
     * @returns {ILocation[]} eliglocations The possible Locations which
     * satisfiy the selected Moods
     * @param {Promise<ILocation[]>} eliglocations The eligible locations ->
     * all within a radius
     * @param {number} minAmount The min amount of Locations which
     * should be returned
     * @param {number} maxAmount The max amount of Locations which should
     * be returned
     */
    private filterByMoods(eliglocations: ILocation[], minAmount: number, maxAmount: number): ILocation[] {
        let possibleLoc: ILocation[] = [];

        // Behindertengerecht müssen alle Orte sein
        if (this._moods.includes(Moods.handicap)) {
            eliglocations.forEach((element) => {
                if (element.tags.includes(Moods.handicap)) {
                    possibleLoc.push(element);
                }
            });
        } else {
            possibleLoc = eliglocations;
            if (possibleLoc.length > 1) {
                possibleLoc.sort((a, b) => b.relevance - a.relevance);
            }
        }

        // Hier wird sichergestellt, dass alle Moods erfüllt werden
        const locationsSatisfy = this.satisfyMoods(_.clone(this._moods), possibleLoc, maxAmount);
        const unselectedLocations = locationsSatisfy.unselectedLocs;
        let selectedLocations = locationsSatisfy.selectedLocs;

        // Hier werden weitere Locations hinzugefügt, die zu den ausgewählten Moods passen
        if (selectedLocations.length < maxAmount) {
            selectedLocations = this.selectMoreLocations(selectedLocations, maxAmount, unselectedLocations);

            for (const tmplocation of selectedLocations) {
                const loc = unselectedLocations.find((e) => e.locationid === tmplocation.locationid);
                if (loc !== undefined) {
                    unselectedLocations.splice(unselectedLocations.indexOf(loc), 1);
                }
            }
        }

        // Ist die Min. Anzahl an Locations immernoch nicht erreicht,
        // werden noch Locations hinzugefügt,
        // die nicht mit den Moods übereinstimmen
        if (selectedLocations.length < minAmount) {
            const missingAmount = minAmount - selectedLocations.length;
            selectedLocations.push(...this.getRandom(unselectedLocations, missingAmount));
        }
        return _.shuffle(selectedLocations);
    }

    /**
     * Function which checks, that all of the selected Moods are satisfied.
     * Every Mood at least one time in the Locations
     *
     * @function
     * @private
     * @author jan_koerner
     * @param {Moods[]} moodsToBeSatisfied The selected Moods
     * @param {ILocation[]} possibleLoc Locations which can be selected
     * @param {number} maxAmount The max Amount of Locations
     * @returns  {ILocation[]} Gibt Liste mit
     * Locations zurück,  die zu den Moods passen + Liste mit Locations,
     * die nicht zu den Moods passen, oder die Moods schon in 'possibleLoc'
     * vorkommen
     */
    private satisfyMoods(
        moodsToBeSatisfied: Moods[],
        possibleLoc: ILocation[],
        maxAmount: number
    ): { unselectedLocs: ILocation[]; selectedLocs: ILocation[] } {
        const selectedlocs: ILocation[] = [];
        const unselectedlocs: ILocation[] = [];
        for (const location of possibleLoc) {
            if (
                location.tags.some((r) => {
                    if (moodsToBeSatisfied.includes(r) || moodsToBeSatisfied.length == 0) {
                        return true;
                    }
                    return false;
                })
            ) {
                const satisfiedMoods = _.intersection<Moods>(moodsToBeSatisfied, location.tags);
                for (const mood of satisfiedMoods) {
                    moodsToBeSatisfied.splice(moodsToBeSatisfied.indexOf(mood), 1);
                }
                if (selectedlocs.length < maxAmount) {
                    selectedlocs.push(location);
                    this.removeSatisfiedMoods(location.tags);
                } else {
                    break;
                }
            } else {
                unselectedlocs.push(location);
            }
        }
        return { selectedLocs: selectedlocs, unselectedLocs: unselectedlocs };
    }
    /**
     * Funktion die schaut, dass die restlichen Locations auch zu den Moods
     * passen, wenn max Anzahl an Locations noch nicht erreicht ist
     *
     * @function
     * @private
     * @author jan_koerner
     * @param {number[]} selectedIds The ids of the already selected Locations
     * @param {ILocation[]} locs Locations which arent selected
     * @param {number} maxAmount The max Amount of Locations
     * @returns {ILocation[]} Gibt Liste mit Locations zurück,
     * die zu den Moods passen
     */

    private selectMoreLocations(selectedLocations: ILocation[], maxAmount: number, locs: ILocation[]): ILocation[] {
        const morelocations: ILocation[] = _.clone(selectedLocations);
        if (selectedLocations.length < maxAmount) {
            for (const location of locs) {
                if (
                    location.tags.some((r) => {
                        if (this._moods.includes(r)) {
                            return true;
                        }
                        return false;
                    })
                ) {
                    if (morelocations.length < maxAmount) {
                        morelocations.push(location);
                        this.removeSatisfiedMoods(location.tags);
                    } else {
                        break;
                    }
                }
            }
        }
        return morelocations;
    }

    /**
     * Function that removes Moods if they are satisfied. If you
     * chose "food" you just want one Location with the food-tag,
     * not all of them
     *
     * @function
     * @private
     * @author jan_koerner
     * @param {Moods[]} satisfiedMoods The Moods which are satisfied with
     * the selected Location
     * @returns {void} Emptyness
     */

    private removeSatisfiedMoods(satisfiedMoods: Moods[]): void {
        const fastSatisfiedMoods: Moods[] = [
            Moods.bakery,
            Moods.coffee,
            Moods.dancing,
            Moods.food,
            Moods.party,
            Moods.pizza,
            Moods.sweet
        ];

        const removeableMoods = _.intersection(satisfiedMoods, fastSatisfiedMoods);

        for (const removeableMood of removeableMoods) {
            if (this._moods.includes(removeableMood)) {
                this._moods.splice(this._moods.indexOf(removeableMood), 1);
            }
        }
    }
    /**
     * Function to return a random amount of Locations from a List
     *
     * @function
     * @private
     * @author jan_koerner & valentin_lechner
     * @param {ILocation[]} arr The List of Locations
     * @param {number} n The amount of Locations which should be returned
     * @returns {ILocation[]} List of Random Locations
     */

    private getRandom(arr: ILocation[], n: number): ILocation[] {
        const result = new Array(n);
        let len = arr.length;
        const taken = new Array(len);
        if (n > len) {
            return arr;
        }
        while (n--) {
            const x = Math.floor(Math.random() * len);
            result[n] = arr[taken.includes(x) ? taken[x] : x];
            taken[x] = taken.includes(--len) ? taken[len] : len;
        }
        return result;
    }
    /**
     * Function get the min Amount of Locations which should be returned to the DateLocation Screen
     * Based in the duration
     *
     * @function
     * @private
     * @author jan_koerner & valentin_lechner
     * @param {number} amountLocations the amount of all locations
     * @returns {number} The min Amount
     */
    private getMinAmountLocations(amountLocations: number): number {
        let minAmount: number;
        switch (this._duration) {
            case TourDuration.short:
                minAmount = 3;
                break;
            case TourDuration.medium:
                minAmount = 6;
                break;
            case TourDuration.long:
                minAmount = 9;
                break;
            default:
                console.error('Ungültiger Typ in DateLocationHandler.getMinAmountLocations:', this._duration);
                return -1;
        }
        if (minAmount < amountLocations) {
            if (this._surpriseFactor === SurpriseFactor.full) {
                return Math.floor(minAmount * 0.7);
            }
            return minAmount;
        }

        return amountLocations / 2;
    }
    /**
     * Function get the max Amount of Locations which should be returned to the
     * `DateLocation Screen``
     * Based in the duration
     *
     * @function
     * @private
     * @author jan_koerner & valentin_lechner
     * @returns {number} The max Amount
     */
    private getMaxAmountLocations(): number {
        let maxAmount: number = -1;
        switch (this._duration) {
            case TourDuration.short:
                maxAmount = 6;
                break;
            case TourDuration.medium:
                maxAmount = 9;
                break;
            case TourDuration.long:
                maxAmount = 12;
                break;
            default:
                console.error('Ungültiger Typ in DateLocationHandler.getRadius', this._duration);
                maxAmount = -1;
        }
        return maxAmount;
    }
    /**
     * Function which returns the radius in m,
     * where the Locations should be located
     * Based on the duration of the tour
     *
     * @function
     * @private
     * @author jan_koerner & valentin_lechner
     * @returns {number} The radius in m
     */
    private getRadius(): number {
        switch (this._duration) {
            case TourDuration.short:
                return 3000;
            case TourDuration.medium:
                return 6000;
            case TourDuration.long:
                return 10000;
            default:
                console.error('Ungültiger Typ in DateLocationHandler.getRadius', this._duration);
                return -1;
        }
    }
}
