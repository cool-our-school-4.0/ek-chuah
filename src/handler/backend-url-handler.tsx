import { defConfigHandler } from './config-file-handler';
import { ConfigEnum } from '../enums/enums';
export class BackendUrlHandler {
    public getBackendUrl(): string {
        return defConfigHandler.getEntryByName(ConfigEnum.backendApiUrl);
    }
}

export const defBackendUrlHandler = new BackendUrlHandler();
