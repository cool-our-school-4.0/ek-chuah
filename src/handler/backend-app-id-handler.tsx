import { defConfigHandler } from './config-file-handler';
import { ConfigEnum } from '../enums/enums';

export class BackendAppIdHandler {
    public getAppId(): string {
        return defConfigHandler.getEntryByName(ConfigEnum.backendAppId);
    }
}
export const defBackendAppIdHandler = new BackendAppIdHandler();
