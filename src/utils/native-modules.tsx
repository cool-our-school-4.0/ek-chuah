import { requireNativeComponent } from 'react-native';

const NavigationView = requireNativeComponent('NavigationView') as unknown;
export default NavigationView;
