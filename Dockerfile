FROM frolvlad/alpine-glibc:glibc-2.31
# **************************************************************************
# Big thank you to sgerrand for providing the glibc package:
# https://github.com/sgerrand/alpine-pkg-glibc
# and also, for frolvlad for creating the image on https://hub.docker.com/r/frolvlad/alpine-glibc
# **************************************************************************
LABEL maintainer="Valentin Lechner <valentin_lechner@dismail.de>"

ARG SONARVER=4.3.0.2102
ARG SONARBASEURL="https://binaries.sonarsource.com/Distribution"
ARG SONARPKG="sonar-scanner-cli"
ARG SDKVER=28
ARG BUILDVER=28.0.3
ARG CLITOOLSVER=linux-6200805_latest

RUN apk add --update curl openjdk8 yarn nodejs npm

RUN curl -s -L $SONARBASEURL/$SONARPKG/$SONARPKG-$SONARVER.zip -o scanner.zip \
    && unzip scanner.zip -d /bin/ \
    && rm -rf scanner.zip \
    && chmod +x /bin/sonar-scanner-$SONARVER/bin/sonar-scanner

# adding sonar-scanner to the path
ENV PATH $PATH:/bin/sonar-scanner-$SONARVER/bin
ENV ANDROID_HOME /opt/Android/Sdk
ENV PATH ${PATH}:${ANDROID}/tools:${ANDROID_HOME}/platform-tools

RUN mkdir -p ${ANDROID_HOME} && cd /opt \
    && curl -o SDKTools.zip https://dl.google.com/android/repository/commandlinetools-${CLITOOLSVER}.zip \
    && unzip SDKTools.zip -d ${ANDROID_HOME} \
    && rm -rf SDKTools.zip

RUN yes | ${ANDROID_HOME}/tools/bin/sdkmanager --licenses --sdk_root=${ANDROID_HOME}
RUN ${ANDROID_HOME}/tools/bin/sdkmanager --install "platforms;android-${SDKVER}" "build-tools;${BUILDVER}" --sdk_root=${ANDROID_HOME}
